<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        <x-profession-icon class="w-8 h-8 mr-4" :name="$recipe->profession->name" />
        {{ $recipe->profession->name }} - {{ __('Recipe') }}: {{ $recipe->name }}
    </div>
</x-slot>

<div class="p-4">

    @if ($recipe->description)
    <p>{{ $recipe->description }}</p>
    @endif

    @if ($recipe->craft)
    <div class="mb-4">
        <x-section-heading>
            <h2 class="text-3xl">{{ __('Crafted item') }}</h2>
        </x-section-heading>
        <p class="px-4 space-y-2">
            @if ($recipe->craft->external_id)
            <x-wowhead-link wh-id="{{ $recipe->craft->external_id }}">{{ $recipe->craft->name }}</x-wowhead-link>
            @else
            {{ $recipe->craft->name }}
            @endif
        </p>
    </div>
    @endif

    <div class="mb-4">
        <x-section-heading>
            <h2 class="text-3xl">{{ __('Reagents') }}</h2>
        </x-section-heading>
        <ul class="px-4 space-y-2">
            @foreach($recipe->reagents as $reagent)
            <li>
                <strong>{{ $reagent->pivot->quantity }}</strong>x
                @if ($reagent->external_id)
                <x-wowhead-link wh-id="{{ $reagent->external_id }}">{{ $reagent->name }}</x-wowhead-link>
                @else
                {{ $reagent->name }}
                @endif
            </li>
            @endforeach
        </li>
    </div>

    <x-section-heading>
        <h2 class="text-3xl">{{ __('Crafters') }}</h2>
    </x-section-heading>
    <ul class="mx-4 flex flex-wrap gap-4">
        @foreach($recipe->crafters as $crafter)
        <li>
            <x-character-link class="inline-block" :character="$crafter" href="{{ route('character.profession.show', ['character' => $crafter, 'profession' => $recipe->profession ])}}" />
        </li>
        @endforeach
    </ul>
</div>

</x-layout>
