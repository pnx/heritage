<x-layout name="app">

    <x-slot name="title">{{ __('Recipes') }}</x-slot>

    <div class="p-4">
        <livewire:recipes />
    </div>
</x-layout>
