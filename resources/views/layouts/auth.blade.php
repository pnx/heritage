<x-layout class="bg-gray-100">

    <div class="mx-auto max-w-sm mt-48 border rounded p-4 bg-white">

        <x-notifications />

        {{ $slot }}
    </div>

</x-layout>
