<x-layout name="app">

<x-slot name="title">
    {{ __('Error') }} - {{ $title }}
</x-slot>

<div class="py-12 text-center text-gray-500">

    @if (isset($icon))
    <x-icon :name="$icon" class="h-24 w-24 mx-auto {{ $icon_color ?? 'text-red-500' }} mb-4" />
    @endif

    <h1 class="text-3xl mb-4">{{ $title }}</h1>
    <p>{{ $slot }}</p>
</div>

</x-layout>
