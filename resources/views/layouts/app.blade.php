<x-layout class="bg-gray-200">

    @include('partials.navigation')

    <!-- Page title starts -->
    <div class="relative z-10 bg-gray-800 pt-8 pb-16">
        <div class="max-w-7xl px-6 mx-auto flex items-start justify-between">
            <div>
                <h4 class="text-2xl font-bold leading-tight text-white">
                    {{ $title }}
                </h4>
            </div>

            @if (isset($page_links))
            <div class="flex space-x-2">
                {{ $page_links }}
            </div>
            @endif
        </div>
    </div>
    <!-- Page title ends -->

    <!-- Main content -->
    <div class="mx-4">
        <main class="max-w-7xl min-h-16 mx-auto -mt-8 relative z-10 rounded shadow bg-white">

            <x-notifications />

            {{ $slot }}
        </main>
    </div>
    <!-- Main content ends -->

    @include('partials.footer')

</x-layout>
