<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @livewireStyles
</head>
<body {{ $attributes->merge(['class' => 'antialiased']) }}>

{{ $slot }}

<!-- Scripts -->
@livewireScripts
<x-wowhead-script />
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</body>
</html>
