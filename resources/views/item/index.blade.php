<x-layout name="app">

    <x-slot name="title">{{ __('Items') }}</x-slot>

    <div class="p-4">
        <livewire:items />
    </div>
</x-layout>
