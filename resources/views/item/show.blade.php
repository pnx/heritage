<x-layout name="app">

    <x-slot name="title">{{ __('Item') }} - {{ $item->name }}</x-slot>

    <div class="p-4">
        <div class="mb-4">
            <x-wowhead-link wh-id="{{ $item->external_id }}">{{ $item->name }}</x-wowhead-link>
        </div>

        @if (count($item->recipes) > 0)
        <div class="mb-4">
            <x-section-heading>
                <h2 class="text-3xl">{{ __('Created by') }}</h2>
            </x-section-heading>
            <ul class="px-4 space-y-2">
                @foreach($item->recipes as $recipe)
                <li>
                    @if ($recipe->spell_id)
                    <x-wowhead-link type="spell" wh-id="{{ $recipe->spell_id }}" href="{{ route('recipe.show', [ 'recipe' => $recipe ]) }}">{{ $recipe->name }}</x-wowhead-link>
                    @else
                    {{ $recipe->name }}
                    @endif
                </li>
                @endforeach
            </li>
        </div>
        @endif

        @if (count($item->reagent_for) > 0)
        <div class="mb-4">
            <x-section-heading>
                <h2 class="text-3xl">{{ __('Reagent for') }}</h2>
            </x-section-heading>

            <ul class="px-4 space-y-2">
                @foreach($item->reagent_for as $recipe)
                <li><x-recipe-link :recipe="$recipe" /></li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>


</x-layout>
