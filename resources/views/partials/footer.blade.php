<footer class="max-w-7xl mx-auto px-8 py-4">
    <p class="text-center text-gray-500 text-sm">
        Copyright &copy; 2021 - <x-link href="https://www.shufflingpixels.com" target="_blank">shufflingpixels.com</x-link>
    </p>
</footer>
