
<!-- Navigation starts -->
<nav class="w-full mx-auto bg-gray-800 border-b border-gray-700 shadow relative z-20">
    <div class="max-w-7xl px-4 mx-auto h-16 flex justify-between">
        <ul class="flex items-center space-x-2">
            <!-- Logo -->
            <li aria-label="Home" role="img" class="cursor-pointer">
                <a href="{{ route('home') }}">
                    <img class="object-fill h-14" src="{{ url('images/logo.png') }}" />
                </a>
            </li>

            <x-nav-link href="{{ route('home') }}">{{ __('Roster') }}</x-nav-link>
            <x-nav-link href="{{ route('recipe.index') }}">{{ __('Recipes') }}</x-nav-link>
            <x-nav-link href="{{ route('item.index') }}">{{ __('Items') }}</x-nav-link>

            @can("administrate")
            <x-nav-link href="{{ route('admin.users.index') }}">{{ __('Admin') }}</x-nav-link>
            @endcan
        </ul>

        <div class="flex items-center justify-end text-white space-x-4">
            @auth
                <a href="{{ route('profile.index') }}"><strong>{{ auth()->user()->username }}</strong></a>
                <x-form method="DELETE" action="{{ route('auth.logout') }}">
                    <a href="/" onclick="this.closest('form').submit();return false;">{{ __('Logout') }}</a>
                </x-form>
            @else
                <a href="{{ route('auth.login') }}">{{ __('Login') }}</a>
            @endauth

            <div class="flex space-x-2">
            @foreach(config('lang') as $locale => $name)
                <a href="{{ route('locale.store', ['locale' => $locale ]) }}" title="{{ $name }}">
                    <x-icon class="w-6 h-6" name="flags.{{ $locale }}" />
                </a>
            @endforeach
            </div>

        </div>
    </div>
</nav>
<!-- Navigation ends -->
