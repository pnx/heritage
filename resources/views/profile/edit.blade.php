<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('Edit User') }} - {{ $user->username }}
    </div>
</x-slot>

<div class="mx-auto max-w-sm p-4">

    <x-form action="{{ route('profile.update') }}">

        <div class="mb-2">
            <x-form.label for="username">{{ __('Username') }}</x-form.label>
            <x-input name="username" :value="$user->username" />
        </div>

        <div class="mb-2">
            <x-form.label for="character_id">{{ __('Main character') }}</x-form.label>
            <x-select name="character_id" :value="$user->character_id" :options="$user->characters->pluck('name', 'id')"/>
        </div>

        <x-section-heading>
            <h2 class="text-2xl">Password</h2>
        </x-section-heading>

        <div class="mb-2">
            <x-form.label for="current_password">{{ __('Current Password') }}</x-form.label>
            <x-input-password name="current_password" />
        </div>

        <div class="mb-2">
            <x-form.label for="password">{{ __('New Password') }}</x-form.label>
            <x-input-password name="password" />
        </div>

        <div class="mb-2">
            <x-form.label for="password_confirmation">{{ __('Confirm New Password') }}</x-form.label>
            <x-input-password name="password_confirmation" />
        </div>

        <div class="mt-2">
            <x-button element="input" type="submit" value="Save" />
        </div>
    </x-form>
</div>

</x-layout>
