<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('User') }} - {{ $user->username }}
    </div>
</x-slot>

<x-slot name="page_links">
    <x-button element="a" class="flex items-center" href="{{ route('profile.edit') }}">
        <x-icon name="pencil" class="h-6 w-6 mr-1" /> {{ __('Edit') }}
    </x-button>
</x-slot>

<div class="p-4">

    <x-section-heading>
        <h2 class="text-3xl">{{ __('Characters') }}</h2>
        <div class="flex space-x-2">
            <x-button element="a" class="flex items-center" href="{{ route('character.create') }}">
                <x-icon name="plus" class="h-6 h-6 mr-1"/> {{ __('Add character') }}
            </x-button>

            <x-button element="a" class="flex items-center" href="{{ route('profession.create') }}">
                <x-icon name="plus" class="h-6 h-6 mr-1"/> {{ __('Import profession') }}
            </x-button>
        </div>
    </x-section-heading>

    @if ($user->characters->count())
    <div class="grid md:grid-cols-2 lg:grid-cols-4 gap-4">
        @foreach($user->characters as $character)
        <x-character-card :character="$character" destroy="{{ route('character.destroy', [ 'character' => $character->slug ])}}"/>
        @endforeach
    </div>
    @else
    <x-empty-result title="No characters">Please add some of yours!</x-empty-result>
    @endif
</div>

</x-layout>
