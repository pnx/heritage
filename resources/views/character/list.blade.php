<x-layout name="app">

<x-slot name="title">{{ __('Roster') }}</x-slot>

<div class="grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 p-4 gap-4">
    @foreach($items as $item)
    <x-character-card :character="$item" />
    @endforeach
</div>

</x-layout>
