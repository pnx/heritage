<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('Character') }} -
        <x-class-icon class="w-8 h-8 mx-2" :name="$character->class" />
        {{ $character->level }} {{ $character->name }}
        - {{ __('Edit') }}
    </div>
</x-slot>

<div class="p-6 mx-auto max-w-xl">
    <livewire:form.character-form :character="$character" />
</div>

</x-layout>
