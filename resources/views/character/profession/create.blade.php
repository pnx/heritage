<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        <x-class-icon class="w-8 h-8 mr-2" :name="$character->class" />
        {{ $character->name }}
        -
        {{ __('Import Profession') }}
    </div>
</x-slot>

<div class="p-4">
    <x-profession-import-form route="{{ route('character.profession.store', [ 'character' => $character->slug ]) }}" />
</div>

</x-layout>
