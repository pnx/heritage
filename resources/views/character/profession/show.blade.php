<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('Character') }} -
        <div class="flex mx-2 space-x-1">
            <x-race-icon class="w-8 h-8" :character="$ch_prof->character" />
            <x-class-icon class="w-8 h-8" :name="$ch_prof->character->class" />
        </div>

        <a href="{{ route('character.show', ['character' => $ch_prof->character ]) }}">
            {{ $ch_prof->character->level }} {{ $ch_prof->character->name }}
        </a>
    </div>
</x-slot>

<div class="p-4">

    <x-section-heading>
        <div class="flex items-end">
            <x-profession-icon :name="$ch_prof->profession->name" class="h-8 w-8 mr-2"/>
            <h2 class="text-3xl">
                @if ($ch_prof->specialization)
                {{ $ch_prof->profession->name }}: {{ $ch_prof->specialization->name }}
                @else
                {{ $ch_prof->profession->name }}
                @endif
            </h2>
            <h3 class="text-xl ml-2">{{ $ch_prof->skill }}</h3>
        </div>
    </x-section-heading>


    @php
    $recipes = $ch_prof->recipes
        ->sortBy('craft.name')
        ->groupBy('category.name')
        ->sortKeys();
    @endphp

    @foreach($recipes as $category => $recipes)
    <span x-data="{ open: true }">
        <h2 class="flex items-center my-2 text-2xl cursor-pointer hover:text-gray-600" @click="open = ! open">
            <x-icon name="chevron-right" class="h-6 w-6" x-show="!open" x-cloak />
            <x-icon name="chevron-down" class="h-6 w-6" x-show="open" />
            {{ $category }}
        </h2>
        <ul class="ml-2 space-y-2" x-show="open" x-transition>
            @foreach($recipes as $recipe)
            <li>
                @php
                    if ($recipe->spell) {
                        $id = $recipe->spell->id;
                        $type = 'spell';
                    } else {
                        $id = $recipe->craft->external_id;
                        $type = 'item';
                    }
                @endphp
                <x-wowhead-link :wh-id="$id" :type="$type" href="{{ route('recipe.show', [ 'recipe' => $recipe ]) }}">{{ $recipe->name }}</x-wowhead-link>
            </li>
            @endforeach
        </ul>
    </span>
    @endforeach

</div>

</x-layout>
