<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('Character') }} -
        <div class="flex mx-2 space-x-1">
            <x-race-icon class="w-8 h-8" :character="$character" />
            <x-class-icon class="w-8 h-8" :name="$character->class" />
        </div>
        {{ $character->level }} {{ $character->name }}
    </div>
</x-slot>

@can('update', $character)
<x-slot name="page_links">
    <x-button element="a" class="flex items-center" href="{{ route('character.edit', [ 'character' => $character ]) }}">
        <x-icon name="pencil" class="h-6 h-6 mr-1"/> {{ __('Edit') }}
    </x-button>
</x-slot>
@endcan

<div class="p-4">

    <x-section-heading>
        <h2 class="text-3xl">{{ __('Owner') }}</h2>
    </x-section-heading>

    <x-link href="{{ route('user.show', [ 'user' => $character->user ])}}">
        {{ $character->user->username }}
    </x-link>

    <x-section-heading>
        <h2 class="text-3xl">{{ __('Professions') }}</h2>
        <div>
            @can('import_profession', $character)
            <x-button element="a" class="flex items-center" href="{{ route('character.profession.create', [ 'character' => $character->slug ]) }}">
                <x-icon name="plus" class="h-6 h-6 mr-1"/> {{ __('Import') }}
            </x-button>
            @endcan
        </div>
    </x-section-heading>

    <ul class="space-y-2">
        @foreach($character->professions as $profession)
        <li class="flex items-center">
            <x-profession-icon :name="$profession->name" class="h-8 w-8 mr-2"/>
            <x-link href="{{ route('character.profession.show', [ 'character' => $character->slug, 'profession' => $profession->slug ]) }}">
                {{ $profession->name }}
            </x-link>
            <span class="ml-1">@ {{ $profession->skill }}</span>
        </li>
        @endforeach
    </ul>
</div>

</x-layout>
