<x-layout name="app">

<x-slot name="title">{{ __('Add character') }}</x-slot>

<div class="p-6 mx-auto max-w-xl">
    <livewire:form.character-form />
</div>

</x-layout>
