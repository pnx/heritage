<x-layout name="error">

<x-slot name="icon">shield-exclamation</x-slot>
<x-slot name="title">{{ __('Unauthorized') }}</x-slot>

{{ __($exception->getMessage() ?: 'You are not allowed to access this page.') }}

</x-layout>
