<x-layout name="error">

<x-slot name="icon">fire</x-slot>
<x-slot name="title">{{ __('Server Error') }}</x-slot>

{{ __('The server encountered an error, please try again.') }}

</x-layout>
