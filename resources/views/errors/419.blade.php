<x-layout name="error">

<x-slot name="icon">clock</x-slot>
<x-slot name="icon_color">text-blue-400</x-slot>
<x-slot name="title">{{ __('Page expired.') }}</x-slot>

{{ __('The page has expired. Please reload and try again.') }}

</x-layout>
