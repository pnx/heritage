<x-layout name="error">

<x-slot name="icon">search</x-slot>
<x-slot name="icon_color">text-blue-400</x-slot>
<x-slot name="title">{{ __('Page not found') }}</x-slot>

The page you are trying to access does not exist.

</x-layout>
