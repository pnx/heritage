<x-layout name="error">

<x-slot name="icon">exclamation-circle</x-slot>
<x-slot name="icon_color">text-blue-400</x-slot>
<x-slot name="title">{{ __('Service Unavailable') }}</x-slot>

{{ __('The service is not available at the moment, please try again later.') }}

</x-layout>
