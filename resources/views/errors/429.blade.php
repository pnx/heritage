<x-layout name="error">

<x-slot name="icon">exclamation</x-slot>
<x-slot name="title">{{ __('Too Many Requests') }}</x-slot>

{{ __('You have made too many requests under a short period of time. Please wait abit and try again.') }}

</x-layout>
