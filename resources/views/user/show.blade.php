<x-layout name="app">

    <x-slot name="title">{{ __('User') }} - {{ $user->username }}</x-slot>

    <div class="p-4">

        <x-section-heading>
            <h2 class="text-3xl">{{ __('Characters') }}</h2>
        </x-section-heading>

        @if ($user->characters->count())
        <div class="grid md:grid-cols-2 lg:grid-cols-4 gap-4">
            @foreach($user->characters as $character)
            <x-character-card :character="$character" :frame="$character->isMain()" />
            @endforeach
        </div>
        @else
        <x-empty-result title="No characters"></x-empty-result>
        @endif
    </div>
</x-layout>
