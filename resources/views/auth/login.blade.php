<x-layout name="auth">

    <h1 class="text-gray-500 text-3xl text-center mb-4">{{ __('Login') }}</h1>

    <x-form action="{{ route('auth.login.store') }}">
        <x-input name="username" placeholder="{{ __('Username') }}" class="mb-2"/>
        <x-input-password placeholder="{{ __('Password') }}" class="mb-2" />
        <x-button element="input" type="submit" name="submit" value="{{ __('Login') }}" class="w-full" />
    </x-form>

    @if (config('services.discord.client_id'))
    <x-divider>{{ __('or') }}</x-divider>

    <x-button element="a" href="{{ route('oauth.create', ['driver' => 'discord'])}}" class="text-center w-full">
        <x-icon name="discord-text-white" class="mx-auto h-8" />
    </x-button>
    @endif

</x-layout>
