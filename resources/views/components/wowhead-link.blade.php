<x-link {{ $attributes->merge($component_attr) }}>
    {{ $slot }}
</x-link>
