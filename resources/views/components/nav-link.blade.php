@props(['active' => false])
@php

$classes = 'cursor-pointer px-2 py-1 rounded flex items-center text-sm tracking-normal';

if ($active) {
    $classes .= ' bg-gray-900 text-indigo-300';
} else {
    $classes .= ' text-white hover:text-indigo-300 hover:bg-gray-900';
}

@endphp

<li><a {{ $attributes->merge(['class' => $classes]) }}>{{ $slot }}</a></li>
