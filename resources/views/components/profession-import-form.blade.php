<p class="text-center">
    <strong>{{ __("Important") }}!</strong>
    {{ __('Download this') }}
    <x-link href="https://www.curseforge.com/wow/addons/professions-exporter" target="_blank">{{ __('addon') }}</x-link>
    {{ __('to generate the import string') }}
</p>

<x-form action="{{ $route }}">
    <div class="mb-2">
        <x-form.label for="data">{{ __('Import string') }}</x-form.label>
        <x-textarea class="h-64" name="data" />
    </div>

    <div class="mt-2">
        <x-button element="input" type="submit" value="{{ __('Import') }}" />
    </div>
</x-form>
