@props(['recipe'])
@php
    if ($recipe->spell) {
        $id = $recipe->spell->id;
        $type = 'spell';
    } else {
        $id = $recipe->craft->external_id;
        $type = 'item';
    }
@endphp
<x-wowhead-link :type="$type" :wh-id="$id" href="{{ route('recipe.show', [ 'recipe' => $recipe ]) }}">
    {{ $recipe->name }}
</x-wowhead-link>
