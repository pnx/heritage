@props(['type', 'message'])

@php

switch($type) {
case 'info':
    $border = 'border-blue-300';
    $text = 'text-blue-800';
    $bg = 'bg-blue-200';
    break;
case 'success':
    $border = 'border-green-300';
    $text = 'text-green-800';
    $bg = 'bg-green-200';
    break;
case 'warning':
    $border = 'border-yellow-300';
    $text = 'text-yellow-800';
    $bg = 'bg-yellow-200';
    break;
case 'error':
    $border = 'border-red-300';
    $text = 'text-red-800';
    $bg = 'bg-red-200';
    break;
default:
    $border = 'border-gray-300';
    $text = 'text-gray-800';
    $bg = 'bg-gray-200';
}

@endphp

<div class="px-4 py-2 rounded border {{ $bg }} {{ $text }} {{ $border }}">
    {!! $message !!}
</div>
