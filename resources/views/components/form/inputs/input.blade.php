
<input type="{{ $type }}" name="{{ $name }}" value="{{ $value }}"
    {{ $attributes->merge(['class' => ($errors->has($name) ? 'border-red-400 ' : '') . 'w-full border border-gray-200 rounded px-2 py-1 focus:ring-1']) }} {{ $disabled }} />

@error($name)
<p class="ml-2 text-red-400 text-sm">{{ $message }}</p>
@enderror
