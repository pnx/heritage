<select name="{{ $name }}" {!! $attributes->merge(['class' => 'w-full border border-gray-200 rounded p-1 focus:ring-1']) !!}>
    @forelse($options as $key => $option)
        <option value="{{ $key }}" @if($key == $value)selected @endif>
            {{ $option }}
        </option>
    @empty
        {!! $slot !!}
    @endforelse
</select>

@error($name)
<p class="ml-2 text-red-400 text-sm">{{ $message }}</p>
@enderror
