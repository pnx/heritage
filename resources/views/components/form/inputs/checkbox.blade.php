
<input type="checkbox" name="{{ $name }}" value="{{ $value }}"
    {{ $attributes->merge(['class' => ($errors->has($name) ? 'border-red-400 text-red-400 ' : '') . 'border border-gray-200 rounded p-2 focus:ring-1']) }} {{ $disabled }} />

@error($name)
<p class="ml-2 text-red-400 text-sm">{{ $message }}</p>
@enderror
