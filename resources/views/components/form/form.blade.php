<form {!! $attributes->merge(['method' => $method]) !!}>
@unless(in_array($method, ['HEAD', 'GET', 'OPTIONS']))
    @csrf
@endunless

@if($spoofMethod)
    @method($spoofMethod)
@endif

    {!! $slot !!}
</form>
