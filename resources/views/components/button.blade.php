@props(['element' => 'button'])

<{{ $element }} {{ $attributes->merge(['class' => 'cursor-pointer inline-block text-white bg-indigo-400 px-3 py-1 rounded transition-colors hover:bg-indigo-500']) }}>{{ $slot}}</{{ $element }}>
