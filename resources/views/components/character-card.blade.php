@props(['character', 'destroy' => false, 'frame' => false ])

<div class="rounded p-2 bg-class-{{ $character->class }}">
    <div class="flex justify-between">
        <div class="flex items-center">
            @if ($frame)
            <div class="w-8 h-8">
                <x-class-icon :name="$character->class" />
                <img class="transform -translate-x-2 -translate-y-6 scale-200" src="{{ url('images/elite_frame.png') }}" />
            </div>
            @else
            <x-class-icon class="w-8 h-8" :name="$character->class" />
            @endif
            <a href="{{ route('character.show', ['character' => $character ]) }}" class="text-2xl ml-2">{{ $character->level }} {{ $character->name }}</a>
        </div>

        @if ($destroy)
        <x-form :action="$destroy" method="DELETE">
            <a href="#" onclick="this.closest('form').submit();return false;">
                <x-icon name="cross" class="h-6 h-6 text-gray-100 rounded bg-black bg-opacity-25 hover:text-gray-200 hover:bg-opacity-30 "/>
            </a>
        </x-form>
        @endif
    </div>

    <div class="flex justify-between">
        <p>
            {{ __(Str::ucfirst($character->race)) }}
            {{ __(Str::ucfirst($character->class)) }}
        </p>

        <div class="flex gap-1">
            @foreach($character->professions as $ch_prof)
            <a href="{{ route('character.profession.show', [ 'character' => $character, 'profession' => $ch_prof->profession ]) }}">
                <x-profession-icon class="border border-black rounded-full w-6 h-6" :name="$ch_prof->name" />
            </a>
            @endforeach
        </div>
    </div>
</div>
