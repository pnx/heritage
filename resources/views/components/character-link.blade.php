@props(['href', 'character', 'iconsize' => 5])

<x-link {{ $attributes->merge(['class' => 'flex items-center', 'href' => $href ]) }}>
    <x-class-icon class="w-{{ $iconsize }} h-{{ $iconsize }} mr-1" :name="$character->class" />
    <span>{{ $character->name }}</span>
</x-link>
