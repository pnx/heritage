<a {{ $attributes->merge(['class' => 'text-indigo-400 hover:underline']) }}>{{ $slot}}</a>
