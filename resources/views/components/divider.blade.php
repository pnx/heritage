@if (strlen($slot) > 0)
<div class="flex items-center justify-between my-4">
    <div class="bg-gray-200 h-px w-full"></div>
    <div class="text-gray-400 mx-4 uppercase">{{ $slot }}</div>
    <div class="bg-gray-200 h-px w-full"></div>
</div>
@else
<div class="bg-gray-200 h-px w-full my-6"></div>
@endif
