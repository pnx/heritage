<div class="flex items-end justify-between px-2 py-2 mb-2 border-b">
    {{ $slot }}
</div>
