@if ($messages)
<div class="px-4 pt-4 space-y-2">
    @foreach($messages as $type => $message)
    <x-alert :type="$type" :message=$message />
    @endforeach
</div>
@endif
