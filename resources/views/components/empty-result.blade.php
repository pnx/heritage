@props(['title' => false])
<div {{ $attributes->merge(['class' => 'select-none text-center py-6 border rounded text-gray-500 bg-gray-100 border-gray-200']) }}>
    @if ($title)
    <h2 class="text-3xl mb-2">{{ $title }}</h2>
    @endif
    <p>{{ $slot }}</p>
</div>
