@props(['id', 'type' => 'item'])
<a {{ $attributes->merge([ 'class' => 'text-blue-600', 'href' => sprintf('https://tbc.wowhead.com/%s=%d', $type, $id), 'target' => '_blank' ]) }}>
    <x-icon name="external-link" />
</a>
