<x-layout name="app">

<x-slot name="title">
    <div class="flex">
        {{ __('Import Profession') }}
    </div>
</x-slot>

<div class="p-4">
    <x-profession-import-form route="{{ route('profession.store') }}" />
</div>

</x-layout>
