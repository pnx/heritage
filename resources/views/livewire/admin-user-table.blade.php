<div class="p-4">

    <table class="w-full">

        <tr>
            <th><x-input wire:model="username" name="username" placeholder="Username" /></th>
            <th><x-select wire:model="role" name="role" :options="['' => '-- Role --', 'user' => 'User', 'admin' => 'Admin']" /></th>
        </tr>

        <tr class="border-b-2">
            <th class="py-2 text-left">Username</th>
            <th class="py-2 w-24">Role</th>
            <th class="py-2 w-44">Created</th>
            <th class="py-2 w-44">Updated</th>
            <th class="py-2 w-6">&nbsp;</th>
        </tr>

        @foreach($users as $user)
        <tr class="border-b hover:bg-gray-100">
            <td class="px-2 py-1">
                <x-link href="{{ route('admin.users.edit', [ 'user' => $user ]) }}">
                    {{ $user->username }}
                </x-link>
            </td>
            <td class="px-2 py-1 text-center">{{ $user->role }}</td>
            <td class="px-2 py-1">{{ $user->created_at }}</td>
            <td class="px-2 py-1">{{ $user->updated_at }}</td>
            <td>
                <x-form :action="route('admin.users.destroy', [ 'user' => $user ])" method="DELETE">
                    <a href="#" onclick="this.closest('form').submit();return false;">
                        <x-icon name="cross" class="h-6 h-6 text-danger-400 hover:text-danger-600" />
                    </a>
                </x-form>
            </td>
        </tr>
        @endforeach
    </table>

    <div class="mt-4">
        {{ $users->links() }}
    </div>
</div>
