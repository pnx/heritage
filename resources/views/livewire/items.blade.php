<div>

    <div class="grid grid-cols-2 gap-4 mb-2">
        <x-input wire:model="name" name="name" placeholder="{{ __('Name') }}" />
        <div class="flex items-center">
            <label for="reagent_for" class="mr-2">{{ __('Only reagents') }}</label>
            <x-checkbox wire:model="reagent_for" name="reagent_for" />
        </div>
    </div>

    @if ($items->count())
    <table class="w-full whitespace-nowrap">
        <thead>
            <tr tabindex="0" class="focus:outline-none w-full text-sm leading-none text-gray-800">
                <th class="text-left w-1/4 px-3 py-4">{{ __('Name') }}</th>
                <th class="text-left w-3/4 px-3 py-4">{{ __('Reagent for') }}</th>
            </tr>
        </thead>
        <tbody class="w-full">
            @foreach($items as $item)
            <tr class="focus:outline-none text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100">
                <td class="px-3 py-4">
                    <x-wowhead-link type="item" :wh-id="$item->external_id" href="{{ route('item.show', [ 'item' => $item ]) }}">
                        {{ $item->name }}
                    </x-wowhead-link>
                </td>
                <td class="px-3 py-4">
                    @if ($item->reagent_for && $item->reagent_for->count() > 0)
                    <ul class="flex flex-wrap items-center gap-2">
                        @foreach($item->reagent_for->take(15) as $recipe)
                        <li>
                            <x-recipe-link :recipe="$recipe" />
                        </li>
                        @endforeach

                        @if ($item->reagent_for->count() > 15)
                        <li>and <strong>{{ $item->reagent_for->count() - 15 }}</strong> more</li>
                        @endif
                    </ul>
                    @else
                    <span class="text-gray-400">{{ __('None') }}</span>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="mt-4">
        {{ $items->links() }}
    </div>
    @else
    <x-empty-result>No items found</x-empty-result>
    @endif
</div>
