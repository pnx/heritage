<x-form class="p-4" wire:submit.prevent="save">

    <div>
        <x-form.label for="username">{{ __('Username') }}</x-form.label>
        <x-input wire:model="user.username" name="user.username" label="username" />
    </div>

    <div>
        <x-form.label for="role">{{ __('Role') }}</x-form.label>
        <x-select wire:model="user.role" name="role" :options="['user' => 'User', 'admin' => 'Admin']" />
    </div>


    <div class="mb-2">
        <x-form.label for="password">{{ __('Password') }}</x-form.label>
        <x-input-password wire:model="password" name="password" />
    </div>

    <div class="mb-2">
        <x-form.label for="password_confirmation">{{ __('Confirm Password') }}</x-form.label>
        <x-input-password wire:model="password_confirmation" name="password_confirmation" />
    </div>

    <div class="mt-2">
        <x-button element="input" type="submit" value="{{ $user->id ? 'Save' : 'Create' }}" />
    </div>
</x-form>
