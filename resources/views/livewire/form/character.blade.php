
<x-form wire:submit.prevent="save">

    <div class="grid grid-cols-2 gap-4">

        <div>
            <x-form.label for="name">{{ __('Name') }}</x-form.label>
            <x-input wire:model="character.name" name="character.name" />
        </div>

        <div>
            <x-form.label for="level">{{ __('Level') }}</x-form.label>
            <x-input wire:model="character.level" name="character.level" />
        </div>
    </div>

    <div class="grid grid-cols-3 gap-4 mt-2">
        <div>
            <x-form.label for="race">{{ __('Race') }}</x-form.label>
            <x-select wire:model="character.race" name="character.race" :options="$races" />
        </div>

        <div>
            <x-form.label for="gender">{{ __('Gender') }}</x-form.label>
            <x-select wire:model="character.gender" name="character.gender" :options="$genders" />
        </div>

        <div>
            <x-form.label for="class" >{{ __('Class') }}</x-form.label>
            <x-select wire:model="character.class" name="character.class" :options="$classes" />
        </div>
    </div>

    <div class="mt-2">
        <x-button element="input" type="submit" value="{{ __($this->character->exists ? 'Save' : 'Create') }}" />
    </div>

</x-form>
