<div>

    <div class="grid grid-cols-3 gap-4 mb-2">
        <x-input wire:model="name" name="name" placeholder="{{ __('Recipe') }}" />
        <x-input wire:model="crafter" name="crafter" placeholder="{{ __('Crafter') }}" />
        <x-select wire:model="profession" name="profession" :options="$profession_options" />
    </div>

    @if ($recipes->count())
    <table class="w-full whitespace-nowrap">
        <thead>
            <tr tabindex="0" class="focus:outline-none w-full text-sm leading-none text-gray-800">
                <th class="text-left px-3 py-4">{{ __('Recipe') }}</th>
                <th class="text-left px-3 py-4">{{ __('Profession') }}</th>
                <th class="text-left px-3 py-4">{{ __('Crafters') }}</th>
            </tr>
        </thead>
        <tbody class="w-full">
            @foreach($recipes as $recipe)
            <tr class="focus:outline-none text-sm leading-none text-gray-800 bg-white hover:bg-gray-100 border-b border-t border-gray-100">
                <td class="px-3 py-4">
                    <x-recipe-link :recipe="$recipe" />
                </td>
                <td class="px-3 py-4">
                    <div class="flex items-center">
                        <x-profession-icon class="w-4 h-4 mr-1" :name="$recipe->profession->name" />
                        {{ $recipe->profession->name }}
                    </div>
                </td>
                <td class="px-3 py-4">
                    @if ($recipe->crafters->count() > 0)
                    <ul class="flex flex-wrap gap-2">
                        @foreach($recipe->crafters as $crafter)
                        <li>
                            <x-character-link :character="$crafter" href="{{ route('character.profession.show', ['character' => $crafter, 'profession' => $recipe->profession ]) }}" />
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <span class="text-gray-400">{{ __('None') }}</span>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="mt-4">
        {{ $recipes->links() }}
    </div>
    @else
    <x-empty-result>No recipes found</x-empty-result>
    @endif
</div>
