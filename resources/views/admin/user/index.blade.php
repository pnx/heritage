<x-layout name="app">

<x-slot name="title">{{ __('Admin') }} - {{ __('Users') }}</x-slot>

<x-slot name="page_links">
    <x-button element="a" class="flex items-center" href="{{ route('admin.users.create') }}">
        {{ __('New') }}
    </x-button>
</x-slot>

<livewire:admin-user-table />

</x-layout>
