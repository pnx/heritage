<x-layout name="app">

<x-slot name="title">{{ __('Admin') }} - {{ __('Users') }} - {{ __(isset($model) ? 'Edit' : 'New') }}</x-slot>

@if (isset($model))
<livewire:form.admin.user-form :user="$model" />
@else
<livewire:form.admin.user-form />
@endif

</x-layout>
