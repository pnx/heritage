require('./bootstrap');

// Alpine
import Alpine from 'alpinejs'
window.Alpine = Alpine
Alpine.start()

// Refresh wowhead links after livewire has rendered.
Livewire.hook('message.processed', (message, component) => {
	if (typeof $WowheadPower != 'undefined') {
		$WowheadPower.refreshLinks();
	}
});
