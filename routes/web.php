<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LocaleSessionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CharacterController;
use App\Http\Controllers\CharacterProfessionController;
use App\Http\Controllers\ProfessionController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\ItemController;

use App\Http\Controllers\Auth\SessionController;

use App\Http\Controllers\Admin\UserController as AdminUserController;

require "oauth.php";

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CharacterController::class, 'index'])->name('home');

Route::name('auth.')->group(function () {
    Route::view('/login', 'auth.login')->name('login');
    Route::post('/login', [SessionController::class, 'store'])->name('login.store');
    Route::delete('/logout', [SessionController::class, 'destroy'])->name('logout');
});

//  Public section
// ----------------------------

Route::get('/lang/{locale}', [LocaleSessionController::class, 'store'])->name('locale.store');

Route::prefix('user')->name('user.')->group(function() {
    Route::get('{user}', [UserController::class, 'show'])->name('show');
});

Route::prefix('characters')->name('character.')->group(function() {
    Route::get('create', [CharacterController::class, 'create'])->name('create');
    Route::post('create', [CharacterController::class, 'store'])->name('store');

    // Character
    Route::prefix('{character}')->group(function () {

        // Character Professions
        Route::prefix('professions')->name('profession.')->group(function() {
            Route::get('/create', [CharacterProfessionController::class, 'create'])->name('create');
            Route::post('/', [CharacterProfessionController::class, 'store'])->name('store');
            Route::get('/{profession}', [CharacterProfessionController::class, 'show'])->name('show');
            Route::delete('/{profession}', [CharacterProfessionController::class, 'destroy'])->name('destroy');
        });

        Route::get('/', [CharacterController::class, 'show'])->name('show');
        Route::get('/edit', [CharacterController::class, 'edit'])->name('edit');
        Route::delete('/', [CharacterController::class, 'destroy'])->name('destroy');
    });
});

// Recipes
Route::prefix('recipes')->name('recipe.')->group(function () {
    Route::get('/', [RecipeController::class, 'index'])->name('index');
    Route::get('/{recipe}', [RecipeController::class, 'show'])->name('show');
});

// Items
Route::prefix('items')->name('item.')->group(function () {
    Route::get('/', [ItemController::class, 'index'])->name('index');
    Route::get('/{item}', [ItemController::class, 'show'])->name('show');
});

//  Authenticated section
// ----------------------------
Route::middleware(['auth'])->group(function() {

    // Profession
    Route::prefix('profession')->name('profession.')->group(function() {
        Route::get('/create', [ProfessionController::class, 'create'])->name('create');
        Route::post('/', [ProfessionController::class, 'store'])->name('store');
    });

    Route::prefix('profile')->name('profile.')->group(function () {
        Route::get('/', [ProfileController::class, 'index'])->name('index');
        Route::get('/edit', [ProfileController::class, 'edit'])->name('edit');
        Route::post('/', [ProfileController::class, 'update'])->name('update');
    });

    // Admin
    Route::middleware(['can:administrate'])->prefix('admin')->name('admin.')->group(function () {

        Route::resource('users', AdminUserController::class)->except(['show', 'store', 'update']);
    });
});
