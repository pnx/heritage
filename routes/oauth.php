<?php

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Auth\OAuthController;

$drivers = [
    'discord'
];

Route::prefix('/oauth/{driver}')
    ->where([ 'driver' => collect($drivers)->join('|') ])
    ->name('oauth.')->group(function () {

    Route::get('/', [ OAuthController::class, 'create' ])->name('create');
    Route::get('/callback', [ OAuthController::class, 'store' ])->name('store');
});
