<?php

namespace Database\Factories;

use App\Models\Spell;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class SpellFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Spell::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->unique()->randomNumber(5),
            'name' => $this->faker->unique()->sentence(8),
            'slug' => function (array $attributes) {
                return Str::slug($attributes['name']);
            }
        ];
    }
}
