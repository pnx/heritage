<?php

namespace Database\Factories;

use App\Models\CharacterProfession;
use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Character;
use App\Models\Profession;
use App\Models\Spell;

class CharacterProfessionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CharacterProfession::class;

    /**
     * Array to keep track uf used id's to make
     * sure we generate unique ones.
     */
    protected static $ids = [];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'character_id' => Character::factory(),
            'profession_id' => function (array $attributes) {
                // Abit of a hack to make sure we generate
                // unique character_id,profession_id pairs.
                $ch_id = $attributes['character_id'];
                if (!isset(self::$ids[$ch_id])) {
                    self::$ids[$ch_id] = [];
                }

                $id = self::$ids[$ch_id][] = Profession::whereNotIn('id', self::$ids[$ch_id])->get()->random()->id;
                return $id;
            },
            'specialization_id' => Spell::factory(),
            'skill' => $this->faker->numberBetween(1, 375),
        ];
    }
}
