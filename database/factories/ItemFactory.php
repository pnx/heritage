<?php

namespace Database\Factories;

use App\Models\Item;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'external_id' => $this->faker->unique()->randomNumber(),
            'name' => $this->faker->unique()->sentence(8),
            'slug' => function (array $attributes) {
                return Str::slug($attributes['name']);
            },
            'texture' => $this->faker->numberBetween(1, 999999),
            'color' => Str::of($this->faker->hexcolor)->replace('#', '')
        ];
    }
}
