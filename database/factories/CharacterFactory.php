<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Character;
use App\Models\CharacterProfession;
use App\Warcraft\Classes;

use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Character::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'name' => $this->faker->unique()->firstName(),
            'race' => $this->faker->randomElement(['human', 'dwarf', 'gnome', 'night elf', 'draenei', 'orc', 'troll', 'tauren', 'undead', 'blood elf']),
            'gender' => $this->faker->randomElement(['F', 'M']),
            'class' => function (array $attributes) {
                return (new Classes)->race($attributes['race'])->random();
            },
            'level' => $this->faker->numberBetween(1, 70),
        ];
    }

    public function hasRandomExistingProfessions($count)
    {
        $professions = CharacterProfession::factory()
            ->count($count);

        return $this->has($professions, 'professions');
    }
}
