<?php

namespace Database\Factories;

use App\Models\Profession;
use App\Models\Item;
use App\Models\Spell;
use App\Models\Recipe;
use App\Models\RecipeCategory;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecipeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Recipe::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'profession_id' => Profession::factory(),
            'category_id' => RecipeCategory::factory(),
            'item_id' => Item::factory(),
            'spell_id' => Spell::factory(),
            //'slug' => function (array $attributes) {
            //    return Str::slug(Item::find($attributes['item_id'])->name);
            //}
        ];
    }

    public function randomExistingProfession()
    {
        return $this->state(function (array $attributes) {
            return [
                'profession_id' => Profession::all()->random()->id,
            ];
        });
    }

    public function randomExistingCategory()
    {
        return $this->state(function (array $attributes) {
            return [
                'category_id' => RecipeCategory::all()->random()->id,
            ];
        });
    }

    public function randomExistingItem()
    {
        return $this->state(function (array $attributes) {
            $ids = Recipe::select('item_id')->orderBy('item_id')->get()->pluck('item_id');
            return [
                'item_id' => Item::whereNotIn('id', $ids)->get()->random()->id,
            ];
        });
    }
}
