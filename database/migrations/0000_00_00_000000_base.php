<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Base extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('password');
            $table->enum('role', ['user', 'admin']);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('professions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
        });

        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('external_id')->unique();
            $table->string('name')->unique();
            $table->string('slug')->unique();
            $table->string('color', 9)->nullable();
            $table->integer('texture')->nullable();
        });

        Schema::create('recipe_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
        });

        Schema::create('recipes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('profession_id')->constrained();
            $table->foreignId('category_id')->constrained('recipe_categories');
            $table->foreignId('item_id')->constrained()->comment('crafted item');
            $table->unique(['profession_id', 'item_id']);
        });

        Schema::create('reagents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('recipe_id')->constrained();
            $table->foreignId('item_id')->constrained();
            $table->unsignedTinyInteger('quantity')->default(1);
            $table->unique(['recipe_id', 'item_id']);
        });

        Schema::create('characters', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->string('name', 12)->unique();
            $table->enum('race', ['human', 'dwarf', 'gnome', 'night elf', 'draenei', 'orc', 'troll', 'tauren', 'undead', 'blood elf']);
            $table->enum('gender', ['F', 'M']);
            $table->enum('class', ['warrior', 'hunter', 'rogue', 'mage', 'warlock', 'priest', 'druid', 'shaman', 'paladin']);
            $table->unsignedTinyInteger('level')->default(1);
        });

        Schema::create('character_professions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('character_id')->constrained()->onDelete('cascade');
            $table->foreignId('profession_id')->constrained();
            $table->integer('skill');
            $table->unique(['character_id', 'profession_id']);
        });

        Schema::create('character_profession_recipe', function (Blueprint $table) {
            $table->id();
            $table->foreignId('ch_prof_id')->constrained('character_professions')->onDelete('cascade');
            $table->foreignId('recipe_id')->constrained();
            $table->unique(['ch_prof_id', 'recipe_id']);
        });
    }
}
