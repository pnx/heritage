<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class FixDuplicateRecipies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {

            // Find all new recipes (with spell_id)
            $recipes = DB::table('recipes')
                ->select()
                ->whereNotNull('spell_id')
                ->get();

            foreach($recipes as $recipe) {

                // Get the original row
                $original = DB::table('recipes')
                    ->select()
                    ->where('item_id', $recipe->item_id)
                    ->whereNull('spell_id')
                    ->first();

                // Skip if it didnt exist.
                if (!$original) {
                    continue;
                }

                // Find all character's with the new recipe to the original one.
                DB::table('character_profession_recipe')
                    ->where('recipe_id', $recipe->id)
                    ->update([ 'recipe_id' => $original->id ]);

                // Delete reagents for new recipe
                DB::table('reagents')->where('recipe_id', $recipe->id)->delete();

                // Delete new recipe.
                DB::table('recipes')->where('id', $recipe->id)->delete();

                // Update original with the spell_id
                DB::table('recipes')
                    ->where('id', $original->id)
                    ->update([ 'spell_id' => $recipe->spell_id ]);
            }
        });
    }
}
