<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpellIdToRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->foreignId('spell_id')->after('item_id')->nullable()->constrained();

            // Drop foreign key because we can't drop unique otherwise.
            $table->dropForeign(['item_id']);
            $table->dropForeign(['profession_id']);
            $table->dropUnique(['profession_id', 'item_id']);

            // Add them again and the new unique compound key.
            $table->foreign('item_id')->references('id')->on('items')->constrained();
            $table->foreign('profession_id')->references('id')->on('professions')->constrained();
            $table->unique(['profession_id', 'item_id', 'spell_id']);
        });
    }
}
