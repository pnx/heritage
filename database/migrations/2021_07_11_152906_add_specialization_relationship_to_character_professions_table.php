<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSpecializationRelationshipToCharacterProfessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('character_professions', function (Blueprint $table) {
            $table->foreignId('specialization_id')
                ->after('profession_id')->nullable()->constrained('spells');
        });
    }
}
