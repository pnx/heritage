<?php

namespace Database\Seeders\ProductionSeeders;

use App\Models\Item;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Seed the items table with data from https://github.com/nexus-devs/wow-classic-items database.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::get('https://raw.githubusercontent.com/nexus-devs/wow-classic-items/master/data/json/data.json');

        $data = collect($response->json())
            ->map(function ($item, $key) {
                return [
                    'name' => $item['name'],
                    'slug' => Str::slug($item['name']),
                    'external_id' => $item['itemId']
                ];
            })
            ->chunk(500)
            ->toArray();

        foreach($data as $chunk) {
            Item::upsert($chunk, [ 'name', 'slug' ], [ 'external_id' ]);
        }
    }
}
