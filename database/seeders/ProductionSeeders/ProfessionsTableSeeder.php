<?php

namespace Database\Seeders\ProductionSeeders;

use Illuminate\Database\Seeder;

use App\Models\Profession;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Profession::insert([
            [ 'name' => 'Alchemy' ],
            [ 'name' => 'Blacksmith' ],
            [ 'name' => 'Cooking' ],
            [ 'name' => 'Enchanting' ],
            [ 'name' => 'Engineering' ],
            [ 'name' => 'Jewelcrafting' ],
            [ 'name' => 'Leatherworking' ],
            [ 'name' => 'Tailoring' ]
        ]);
    }
}
