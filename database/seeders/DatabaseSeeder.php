<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductionSeeders\ProfessionsTableSeeder::class);

        $this->call(DevelopmentSeeders\UserTableSeeder::class);
        $this->call(DevelopmentSeeders\ItemTableSeeder::class);
        $this->call(DevelopmentSeeders\RecipeCategoryTableSeeder::class);
        $this->call(DevelopmentSeeders\RecipeTableSeeder::class);
        $this->call(DevelopmentSeeders\CharacterRecipesTableSeeder::class);
    }
}
