<?php

namespace Database\Seeders\DevelopmentSeeders;

use Illuminate\Database\Seeder;

use App\Models\Recipe;
use App\Models\Character;

class CharacterRecipesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach(Character::all() as $character) {

            foreach($character->professions as $ch_prof) {

                $recipes = Recipe::where('profession_id', $ch_prof->profession->id)->get()
                    ->random(rand(10, 50));

                $ch_prof->recipes()->attach($recipes);
            }
        }
    }
}
