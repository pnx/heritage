<?php

namespace Database\Seeders\DevelopmentSeeders;

use Illuminate\Database\Seeder;

use App\Models\RecipeCategory;

class RecipeCategoryTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        RecipeCategory::factory()->count(100)->create();
    }
}
