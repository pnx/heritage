<?php

namespace Database\Seeders\DevelopmentSeeders;

use Illuminate\Database\Seeder;

use App\Models\Item;
use App\Models\Recipe;
use App\Models\RecipeCategory;

class RecipeTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1, 800) as $index) {

            $items = rand(1, 5);
            $quantity = rand(1, 10);

            Recipe::factory()
                ->randomExistingProfession()
                ->randomExistingCategory()
                ->randomExistingItem()
                ->hasAttached(Item::get()->random($items), ['quantity' => $quantity], 'reagents')
                ->create();
        }
    }
}
