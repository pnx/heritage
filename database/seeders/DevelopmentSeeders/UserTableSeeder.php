<?php

namespace Database\Seeders\DevelopmentSeeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Character;

class UserTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'username' => 'admin',
            'role' => 'admin',
        ]);

        $users = User::factory()->count(50)
            ->has(Character::factory()
                ->hasRandomExistingProfessions(2)
                ->count(3))
            ->create();

        foreach($users as $user) {
            $character = $user->characters->random();
            $user->main_character()->associate($character);
            $user->push();
        }
    }
}
