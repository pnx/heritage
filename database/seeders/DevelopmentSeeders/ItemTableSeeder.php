<?php

namespace Database\Seeders\DevelopmentSeeders;

use Illuminate\Database\Seeder;

use App\Models\Item;

class ItemTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Item::factory(1000)->create();
    }
}
