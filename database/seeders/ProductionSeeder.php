<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Seed the application's database with production data.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductionSeeders\ProfessionsTableSeeder::class);
        $this->call(ProductionSeeders\ItemSeeder::class);
    }
}
