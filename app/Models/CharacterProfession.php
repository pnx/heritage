<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class CharacterProfession extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'character_id',
        'profession_id',
        'specialization_id',
        'skill',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'profession'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'name' ];

    /**
     * Relation to the actual profession data.
     */
    public function profession()
    {
        return $this->belongsTo(Profession::class);
    }

    public function specialization()
    {
        return $this->belongsTo(Spell::class, 'specialization_id');
    }

    /**
     * Get the character that has this profession.
     */
    public function character()
    {
        return $this->belongsTo(Character::class);
    }

    /**
     * Get the character's recipes for this profession.
     */
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class,
            'character_profession_recipe', 'ch_prof_id');
    }

    /**
     * Learn a recipe for this character and profession.
     */
    public function learn(Recipe $recipe)
    {
        return $this->recipes()->save($recipe);
    }

    /**
     * Get profession name.
     */
    public function getNameAttribute() : string
    {
        return $this->profession->name;
    }

    public function getSlugAttribute() : string
    {
        return $this->profession->slug;
    }
}
