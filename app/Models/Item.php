<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'external_id',
        'texture',
        'color'
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function ($item) {
            $item->slug = Str::slug($item->name);
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

    /**
     * Get recipes that this item is reagent for.
     */
    public function reagent_for()
    {
        return $this->belongsToMany(Recipe::class, 'reagents')
            ->withPivot('quantity');
    }

    public function getQuantityAttribute()
    {
        if ($this->pivot) {
            return $this->pivot->quantity;
        }
        return null;
    }
}
