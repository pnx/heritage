<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'level',
        'race',
        'gender',
        'class'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function($character) {

            // Unset main character.
            if ($character->isMain()) {
                $character->user->main_character()->dissociate();
                $character->user->save();
            }
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->slug($value)->firstOrFail();
    }

    /**
     * Set the character's name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = Str::ucfirst(Str::lower($value));
    }

    /**
     * Retrieve the user that owns this character.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if this character is the user's main character
     */
    public function isMain()
    {
        return $this->user->character_id == $this->id;
    }

    /**
     * Retrieve the character's professions.
     */
    public function professions()
    {
        return $this->hasMany(CharacterProfession::class);
    }

    public function scopeSlug($q, $name)
    {
        return $q->where('name', Str::ucfirst(Str::lower($name)));
    }

    public function getSlugAttribute()
    {
        return Str::lower($this->name);
    }
}
