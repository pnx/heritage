<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'discord_id',
        'username',
        'password',
        'role',
        'character_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleted(function ($user) {
            $user->characters()->delete();
        });
    }

    public function main_character()
    {
        return $this->belongsTo(Character::class, 'character_id');
    }

    public function alt_characters()
    {
        $relation = $this->hasMany(Character::class);

        if ($this->character_id) {
            $relation->where('id', '!=', $this->character_id);
        }
        return $relation;
    }

    public function characters()
    {
        $relation = $this->hasMany(Character::class);

        // sort main character first if set.
        if ($this->character_id) {
            $relation->orderByRaw('id = ' . $this->character_id . ' DESC');
        }
        return $relation;
    }

    public function getRoleAttribute()
    {
        if (!$this->attributes['role']) {
            return 'user';
        }
        return $this->attributes['role'];
    }
}
