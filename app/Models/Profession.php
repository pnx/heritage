<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->slug($value)->firstOrFail();
    }

    /**
     * Get all characters with this profession.
     */
    public function characters()
    {
        return $this->belongsToMany(Character::class);
    }

    /**
     * What recipes this profession has
     */
    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

    public function scopeSlug($q, $name)
    {
        return $q->where('name', Str::ucfirst($name));
    }

    public function getSlugAttribute()
    {
        return Str::lower($this->name);
    }
}
