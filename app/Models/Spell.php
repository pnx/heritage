<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spell extends Model
{
    use HasFactory;

    public $incrementing = false;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'slug'
    ];

    protected static function boot() {
        parent::boot();

        static::creating(function ($spell) {
            $spell->slug = Str::slug($spell->name);
        });
    }

    public function recipe()
    {
        return $this->hasOne(Recipe::class);
    }
}
