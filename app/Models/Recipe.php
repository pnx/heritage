<?php

namespace App\Models;

use Staudenmeir\EloquentHasManyDeep\HasRelationships as HasDeepRelation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    use Traits\HasSlug;
    use HasDeepRelation;
    use HasFactory;

    public $timestamps = false;

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'profession',
        'craft',
        'spell'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profession_id',
        'category_id',
        'item_id',
        'spell_id',
        'description'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        $spell = $this->whereHas('spell', function ($q) use ($value) {
            $q->where('slug', $value);
        })->first();

        // Fallback on crafted item.
        if (!$spell) {
            return $this->whereHas('craft', function ($q) use ($value) {
                $q->where('slug', $value);
            })->first();
        }

        return $spell;
    }

    /**
     * What profession this item belongs to.
     */
    public function profession()
    {
        return $this->belongsTo(Profession::class);
    }

    /**
     * What category this recipe belongs to.
     */
    public function category()
    {
        return $this->belongsTo(RecipeCategory::class, 'category_id');
    }

    /**
     * What spell this recipe peforms.
     */
    public function spell()
    {
        return $this->belongsTo(Spell::class);
    }

    /**
     * What item this recipe crafts.
     */
    public function craft()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    /**
     * Get all character professions.
     */
    public function character_profession()
    {
        return $this->belongsToMany(CharacterProfession::class,
            'character_profession_recipe', null, 'ch_prof_id');
    }

    /**
     * Get all character that can craft this recipe.
     */
    public function crafters()
    {
        return $this->hasManyDeep(Character::class,
            ['character_profession_recipe', CharacterProfession::class],
            [ null, 'id', 'id'],
            [ null,'ch_prof_id', 'character_id']
        );
    }

    /**
     * What reagents is needed to craft this recipe.
     */
    public function reagents()
    {
        return $this->belongsToMany(Item::class, 'reagents')
            ->withPivot('quantity');
    }

    /**
     * Get recipe name from crafted item or spell.
     */
    public function getNameAttribute()
    {
        if ($this->spell) {
            return $this->spell->name;
        }
        return $this->craft->name;
    }

    /**
     * Get recipe slug from crafted item or spell.
     */
    public function getSlugAttribute()
    {
        if ($this->spell) {
            return $this->spell->slug;
        }
        return $this->craft->slug;
    }
}
