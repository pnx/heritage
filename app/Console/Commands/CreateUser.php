<?php

namespace App\Console\Commands;

use App\Models\User;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {username} {password} {--admin}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $username = $this->argument('username');
        $password = $this->argument('password');

        $role = 'user';
        if ($this->option('admin')) {
            $role = 'admin';
        }

        User::create([
            'username'  => $username,
            'password'  => Hash::make($password),
            'role'      => $role
        ]);

        $this->info('Created');

        return 0;
    }
}
