<?php

namespace App\Warcraft;

use Illuminate\Support\Collection;

class Classes extends Collection
{
    /**
     * Enum constants
     */
    const WARRIOR   = 'warrior';
    const PALADIN   = 'paladin';
    const HUNTER    = 'hunter';
    const ROGUE     = 'rogue';
    const PRIEST    = 'priest';
    const SHAMAN    = 'shaman';
    const MAGE      = 'mage';
    const WARLOCK   = 'warlock';
    const DRUID     = 'druid';

    public function __construct($items = null)
    {
        if ($items === null) {
            $this->items = [
                self::WARRIOR   => 'Warrior',
                self::PALADIN   => 'Paladin',
                self::HUNTER    => 'Hunter',
                self::ROGUE     => 'Rogue',
                self::PRIEST    => 'Priest',
                self::SHAMAN    => 'Shaman',
                self::MAGE      => 'Mage',
                self::WARLOCK   => 'Warlock',
                self::DRUID     => 'Druid'
            ];
        } else {
            parent::__construct($items);
        }
    }

    /**
     * Filter classes by race.
     */
    public function race($race) : self
    {
        switch($race) {
        case Races::HUMAN :
            return $this->only(self::WARRIOR, self::PALADIN, self::ROGUE,
                self::PRIEST, self::MAGE, self::WARLOCK);
        case Races::DWARF :
            return $this->only(self::WARRIOR, self::HUNTER, self::PALADIN,
                self::ROGUE, self::PRIEST);
        case Races::GNOME :
            return $this->only(self::WARRIOR, self::ROGUE, self::MAGE,
                self::WARLOCK);
        case Races::NIGHTELF :
            return $this->only(self::WARRIOR, self::HUNTER, self::ROGUE,
                self::PRIEST, self::DRUID);
        case Races::DRAENEI :
            return $this->only(self::WARRIOR, self::HUNTER, self::PALADIN,
                self::SHAMAN, self::PRIEST, self::MAGE);
        case Races::ORC :
            return $this->only(self::WARRIOR, self::HUNTER, self::ROGUE,
                self::SHAMAN, self::WARLOCK);
        case Races::TROLL :
            return $this->only(self::WARRIOR, self::HUNTER, self::ROGUE,
                self::PRIEST, self::SHAMAN, self::MAGE);
        case Races::TAUREN :
            return $this->only(self::WARRIOR, self::HUNTER, self::SHAMAN,
                self::DRUID);
        case Races::UNDEAD :
            return $this->only(self::WARRIOR, self::ROGUE, self::PRIEST,
                self::MAGE, self::WARLOCK);
        case Races::BLOODELF :
            return $this->only(self::PALADIN, self::HUNTER, self::ROGUE,
                self::PRIEST, self::MAGE, self::WARLOCK);
        }

        return $this;
    }
}
