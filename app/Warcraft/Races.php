<?php

namespace App\Warcraft;

use Illuminate\Support\Collection;

class Races extends Collection
{
    /**
     * Enum constants
     */
    const HUMAN     = 'human';
    const DWARF     = 'dwarf';
    const GNOME     = 'gnome';
    const NIGHTELF  = 'night elf';
    const DRAENEI   = 'draenei';
    const ORC       = 'orc';
    const TROLL     = 'troll';
    const TAUREN    = 'tauren';
    const UNDEAD    = 'undead';
    const BLOODELF  = 'blood elf';

    public function __construct($items = null)
    {
        if ($items === null) {
            $this->items = [
                self::HUMAN     => 'Human',
                self::DWARF     => 'Dwarf',
                self::GNOME     => 'Gnome',
                self::NIGHTELF  => 'Night elf',
                self::DRAENEI   => 'Draenei',
                self::ORC       => 'Orc',
                self::TROLL     => 'Troll',
                self::TAUREN    => 'Tauren',
                self::UNDEAD    => 'Undead',
                self::BLOODELF  => 'Blood elf'
            ];
        } else {
            parent::__construct($items);
        }
    }

    /**
     * Filter only alliance races
     */
    public function alliance() : self
    {
        return $this->only(self::HUMAN, self::DWARF, self::GNOME, self::NIGHTELF, self::DRAENEI);
    }

    /**
     * Filter only alliance races
     */
    public function horde() : self
    {
        return $this->only(self::ORC, self::TROLL, self::TAUREN, self::UNDEAD, self::BLOODELF);
    }

    /**
     * Filter races by class.
     */
    public function class($class) : self
    {
        switch($class) {
        case Classes::WARRIOR :
            return $this->except(self::BLOODELF);
        case Classes::PALADIN :
            return $this->only(self::HUMAN, self::DWARF, self::DRAENEI, self::BLOODELF);
        case Classes::HUNTER :
            return $this->only(self::HUMAN, self::DWARF, self::NIGHTELF, self::DRAENEI,
                self::ORC, self::TROLL, self::TAUREN, self::BLOODELF);
        case Classes::ROGUE :
            return $this->only(self::HUMAN, self::DWARF, self::NIGHTELF, self::GNOME,
                self::ORC, self::TROLL, self::UNDEAD, self::BLOODELF);
        case Classes::PRIEST :
            return $this->only(self::HUMAN, self::DWARF, self::NIGHTELF, self::DRAENEI,
                self::TROLL, self::UNDEAD, self::BLOODELF);
        case Classes::SHAMAN :
            return $this->only(self::DRAENEI, self::ORC, self::TROLL, self::TAUREN);
        case Classes::MAGE :
            return $this->only(self::HUMAN, self::GNOME, self::TROLL, self::UNDEAD, self::BLOODELF);
        case Classes::WARLOCK :
            return $this->only(self::HUMAN, self::GNOME, self::ORC, self::UNDEAD, self::BLOODELF);
        case Classes::DRUID :
            return $this->only(self::NIGHTELF, self::TAUREN);
        }

        return $this;
    }
}
