<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    protected $components = [
        // Form
        'input'             => \App\View\Components\Form\Input::class,
        'input-password'    => \App\View\Components\Form\Password::class,
        'textarea'          => \App\View\Components\Form\Textarea::class,
        'select'            => \App\View\Components\Form\Select::class,
        'checkbox'          => \App\View\Components\Form\Checkbox::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register components
        foreach($this->components as $alias => $class) {
            Blade::component($alias, $class);
        }
    }
}
