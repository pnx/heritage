<?php

namespace App\Http\Livewire\Traits;

trait WithPagination
{
    use \Livewire\WithPagination;

    public $perPage = 15;

    public function paginationView()
    {
        return 'pagination.default';
    }
}
