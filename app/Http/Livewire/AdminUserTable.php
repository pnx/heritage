<?php

namespace App\Http\Livewire;

use App\Models\User;

use Livewire\Component;

class AdminUserTable extends Component
{
    use Traits\WithPagination;

    protected $queryString = [
        'username' => ['except' => ''],
        'role' => ['except' => '']
    ];

    /**
     * Filter by username
     */
    public $username;

    /**
     * Filter by role
     */
    public $role;

    public function render()
    {
        $query = User::query()->orderBy('username');

        if (strlen($this->username) >= 3) {
             $query->where('username', 'LIKE', '%' . $this->username . '%');
        }

        if (in_array($this->role, ['user','admin'])) {
            $query->where('role', $this->role);
        }

        return view('livewire.admin-user-table', [
            'users' => $query->paginate($this->perPage)
        ]);
    }
}
