<?php

namespace App\Http\Livewire\Form;

use App\Models\Character;
use App\Warcraft\Classes;
use App\Warcraft\Races;

use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;

class CharacterForm extends Component
{
    use AuthorizesRequests;

    /**
     * All possible classes
     */
    public $classes;

    /**
     * All possible races
     */
    public $races;

    /**
     * All posible genders
     */
    public $genders = [
        'M' => 'Male',
        'F' => 'Female'
    ];

    /**
     * Character model
     */
    public Character $character;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'character.name'    => [ 'required', 'min:4', Rule::unique('characters', 'name')->ignore($this->character)->whereNotNull('deleted_at') ],
            'character.level'   => [ 'required', 'integer', 'min:1', 'max:70' ],
            'character.gender'  => [ 'required', 'in:M,F' ],
            'character.race'    => [ 'required', 'in:' . $this->races->keys()->join(',') ],
            'character.class'   => [ 'required', 'in:' . $this->classes->keys()->join(',') ]
        ];
    }

    public function mount(Character $character)
    {
        $this->character = $character;
        $this->races = (new Races)->alliance();

        // Set some default values.
        if (!$this->character->exists) {
            $this->character->race = $this->races->keys()->first();
            $this->character->gender = 'M';
            $this->character->level = 70;
        }

        $this->updatedCharacterRace($this->character->race);
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatedCharacterRace($race)
    {
        // Update classes list for this race.
        $this->classes = (new Classes)->race($race);

        // if this race can not be the selected class.
        // select the first one.
        if (!$this->classes->has($this->character->class)) {
            $this->character->class = $this->classes->keys()->first();
        }
    }

    /**
     * Save the character
     */
    public function save()
    {
        if ($this->character->exists) {
            $this->authorize('update', $this->character);
            $action = "updated";
        } else {
            $this->authorize('create', Character::class);
            $action = "created";
        }

        $this->validate();

        if ($action === 'created') {
            $this->character->user()
                ->associate(auth()->user());
        }
        $this->character->save();

        // Livewire redirect() does not have "with" method.
        // so we call session()->flash() directly instead.
        session()->flash('success', "<strong>{$this->character->name}</strong> was {$action}!");
        return redirect()->route('profile.index');
    }

    public function render()
    {
        return view('livewire.form.character');
    }
}
