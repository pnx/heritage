<?php

namespace App\Http\Livewire\Form\Admin;

use App\Models\User;

use Livewire\Component;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserForm extends Component
{
    use AuthorizesRequests;

    /**
     * User object.
     */
    public User $user;

    /**
     * Password field
     */
    public $password;

    /**
     * Password confirmation field.
     */
    public $password_confirmation;

    /**
     * Initialize the component.
     */
    public function mount(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.username'  => ['required', 'min:4', Rule::unique('users', 'username')->ignore($this->user) ],
            'user.role'      => 'required|in:user,admin',
            'password'       => ($this->user->exists ? 'nullable' : 'required') . '|min:8|confirmed',
        ];
    }

    public function updated($property)
    {
        $this->validateOnly($property);
    }

    /**
     * Save the user
     */
    public function save()
    {
        $this->authorize('administrate');

        $this->validate();

        if ($this->password) {
            $this->user->password = Hash::make($this->password);
        }
        $this->user->save();

        // Livewire redirect() does not have "with" method.
        // so we call session()->flash() directly instead.
        session()->flash('success', "<strong>{$this->user->username}</strong> was "
            . ($this->user->exists ? "updated!" : "created!"));
        return redirect()->route('admin.users.index');
    }

    public function render()
    {
        return view('livewire.form.admin.user');
    }
}
