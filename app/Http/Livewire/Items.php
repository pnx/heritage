<?php

namespace App\Http\Livewire;

use App\Models\Item;

use Livewire\Component;

class Items extends Component
{
    use Traits\WithPagination;

    protected $queryString = [
        'name' => ['except' => ''],
        'reagent_for' => ['except' => false],
    ];

    /**
     * Filter by name
     */
    public string $name = '';

    public $reagent_for = false;

    public function render()
    {
        $query = Item::query()
            ->with(['reagent_for']);

        if ($this->reagent_for) {
            $query->has('reagent_for');
        }

        // Filter by name
        if (strlen($this->name) >= 3) {
            $query->where('name', 'LIKE', '%' . $this->name . '%');
        }

        return view('livewire.items', [
            'items' => $query->paginate($this->perPage)
        ]);
    }
}
