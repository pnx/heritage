<?php

namespace App\Http\Livewire;

use App\Models\Profession;
use App\Models\Recipe;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Livewire\Component;

class Recipes extends Component
{
    use Traits\WithPagination;

    protected $queryString = [
        'name' => ['except' => ''],
        'crafter' => ['except' => ''],
        'profession' => ['except' => '']
    ];

    /**
     * Filter by name
     */
    public string $name = '';

    /**
     * Filter by profession
     */
    public string $profession = '';

    /**
     * Filter by crafter
     */
    public string $crafter = '';

    /**
     * List of all professions.
     */
    public array $profession_options = [];

    public function mount()
    {
        // Build professions select list.
        $options = Profession::all()->mapWithKeys(function ($item) {
            return [Str::lower($item['name']) => $item['name']];
        });

        $this->profession_options = collect(['' => '-- ' . __('Profession') . ' --'])
            ->merge($options)
            ->toArray();
    }

    public function render()
    {
        $query = Recipe::select('recipes.*')
            ->with(['crafters'])
            ->leftJoin('items', 'items.id', '=', 'recipes.item_id')
            ->leftJoin('spells', 'spells.id', '=', 'recipes.spell_id')
            ->orderBy(DB::raw('(IF(`recipes`.`spell_id` IS NOT NULL, `spells`.`name`, `items`.`name`))'));

        // Filter by name
        if (strlen($this->name) >= 3) {
            $query->whereHas('spell', function ($q) {
                $q->where('name', 'LIKE', '%' . $this->name . '%');
            })
            ->orWhereHas('craft', function($q) {
                $q->where('name', 'LIKE', '%' . $this->name . '%');
            });
        }

        // Filter by profession
        if (strlen($this->profession)) {
            $query->whereHas('profession', function ($q) {
                $q->where('name', Str::ucfirst($this->profession));
            });
        }

        // Filter by crafter
        if (strlen($this->crafter) >= 3) {
            $query->whereHas('crafters', function ($q) {
                $q->where('name', 'LIKE', '%' . $this->crafter . '%');
            });
        }

        return view('livewire.recipes', [
            'recipes' => $query->paginate($this->perPage)
        ]);
    }
}
