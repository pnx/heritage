<?php

namespace App\Http\Controllers;

use App\Models\Character;
use App\Models\Profession;
use App\Models\Recipe;
use App\Models\CharacterProfession;
use App\Http\Requests\ProfessionImportRequest;
use App\Jobs\ImportProfession;

class CharacterProfessionController extends Controller
{
    public function show(Character $character, Profession $profession)
    {
        $ch_prof = CharacterProfession::where('character_id', $character->id)
            ->where('profession_id', $profession->id)
            ->with(['character', 'recipes.craft', 'recipes.category'])
            ->firstOrFail();

        return view('character.profession.show', [
            'ch_prof' => $ch_prof,
        ]);
    }

    public function create(Character $character)
    {
        $this->authorize('import_profession', $character);

        return view('character.profession.create', [
            'character' => $character
        ]);
    }

    public function store(Character $character, ProfessionImportRequest $request)
    {
        $this->authorize('import_profession', $character);

        $request->validated();

        $data = json_decode($request->input('data'));

        try {
            ImportProfession::dispatch($data, $request->user(), $character);
        } catch(\App\ProfessionImport\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Profession imported!');
    }

    public function destroy(Character $character, Profession $profession)
    {
        $ch_prof = CharacterProfession::where('character_id', $character->id)
            ->where('profession_id', $profession->id)->firstOrFail();

        $this->authorize('delete', $ch_prof);

        $ch_prof->delete();

        return redirect()->back()
            ->with(['success' => "<strong>Profession</strong> was deleted!"]);
    }
}
