<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        return view('profile.index', [
            'user' => Auth::user(),
        ]);
    }

    public function edit(Request $request)
    {
        return view('profile.edit', [
            'user' => Auth::user(),
        ]);
    }

    public function update(UserRequest $request)
    {
        $data = collect($request->validated());

        $user = $request->user();
        $user->fill($data->except('password')->toArray());
        if (strlen($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        $user->save();

        return redirect()->route('profile.index')
            ->with('success', 'Your account was updated!');
    }
}
