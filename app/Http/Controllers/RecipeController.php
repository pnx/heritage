<?php

namespace App\Http\Controllers;

use App\Models\Recipe;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RecipeController extends Controller
{
    /**
     * List all recipies
     */
    public function index()
    {
        return view('recipe.index');
    }

    /**
     * Show a recipe
     */
    public function show(Recipe $recipe)
    {
        $recipe->load([
            'reagents' => function($q) {
                $q->orderBy('name');
            },
            'crafters' => function($q) {
                $q->orderBy('name');
            }
        ]);

        return view('recipe.show', [
            'recipe' => $recipe
        ]);
    }
}
