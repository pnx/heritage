<?php

namespace App\Http\Controllers;

use App\Models\Item;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    /**
     * List all recipies
     */
    public function index()
    {
        return view('item.index');
    }

    /**
     * Show a recipe
     */
    public function show(Item $item)
    {
        return view('item.show', compact('item'));
    }
}
