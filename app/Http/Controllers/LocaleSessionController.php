<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocaleSessionController extends Controller
{
    /**
     * Set locale
     */
    public function store(Request $request, $locale)
    {
        if (array_key_exists($locale, config('lang'))) {
            $request->session()->put('locale', $locale);
        }
        return redirect()->back();
    }
}
