<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfessionImportRequest;
use App\Jobs\ImportProfession;

class ProfessionController extends Controller
{
    public function create()
    {
        return view('profession.create');
    }

    public function store(ProfessionImportRequest $request)
    {
        $request->validated();

        $data = json_decode($request->input('data'));

        try {
            ImportProfession::dispatch($data, $request->user());
        } catch(\App\ProfessionImport\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
        return redirect()->back()->with('success', 'Profession imported!');
    }
}
