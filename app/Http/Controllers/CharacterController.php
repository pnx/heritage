<?php

namespace App\Http\Controllers;

use App\Models\Character;
use App\Http\Requests\CharacterRequest;

class CharacterController extends Controller
{
    public function index()
    {
        return view('character.list', [
            'items' => Character::with('professions')->orderBy('name')->get(),
        ]);
    }

    public function show(Character $character)
    {
        return view('character.show', [
            'character' => $character
        ]);
    }

    public function create()
    {
        $this->authorize('create', Character::class);

        return view('character.create');
    }

    public function edit(Character $character)
    {
        $this->authorize('update', $character);

        return view('character.edit', [
            'character' => $character
        ]);
    }

    public function destroy(Character $character)
    {
        $this->authorize('delete', $character);

        $character->delete();

        return redirect()->back()
            ->with(['success' => "<strong>{$character->name}</strong> was deleted!"]);
    }
}
