<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Show user page
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }
}
