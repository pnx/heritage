<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class OAuthController extends Controller
{
    /**
     * Redirect to OAuth driver.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    /**
     * Handle callback from OAuth driver.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $driver)
    {
        $oauth = Socialite::driver($driver)->stateless()->user();

        // Check that account is not deleted before trying to find/create user and login.
        if (User::onlyTrashed()->where("{$driver}_id", $oauth->getId())->first()) {
            return redirect('/')
                ->with(['error' => "Account suspended."]);
        }

        $user = User::firstOrCreate([ "{$driver}_id" => $oauth->getId() ], [
            'username' => $oauth->getNickname()
        ]);

        Auth::guard('web')->login($user, true);

        $request->session()->regenerate();

        return redirect('/');
    }
}
