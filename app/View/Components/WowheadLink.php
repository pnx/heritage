<?php

namespace App\View\Components;

use Illuminate\View\Component;

/**
 * Render a wowhead tooltip link.
 */
class WowheadLink extends Component
{
    /**
     * Identifier
     */
    public $id;

    /**
     * Type of link
     */
    public $type;

    protected $href;

    /**
     * True if wowhead should rename the link, false if it should leave it alone.
     */
    protected $renameLink;

    /**
     * Posible values: null (default), tiny, small, medium and large
     */
    protected $iconSize;

    /**
     * Wowhead domain to use.
     */
    protected $domain;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($whId, $type = 'item', $href = false,
                                $iconSize = null, $renameLink = null, $domain = null)
    {
        $this->id = $whId;
        $this->type = $type;
        $this->href = $href;
        $this->renameLink = $renameLink;
        $this->iconSize = $iconSize;
        $this->domain = $domain ?? config('wowhead.domain');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $attr = collect([
            'data-wh-rename-link' => $this->renameLink,
            'data-wh-icon-size' => $this->iconSize
        ]);

        // If we have custom url, we must set the
        // 'data-wowhead' attribute with the options.
        if ($this->href) {
            $attr['href'] = $this->href;
            $attr['data-wowhead'] = $this->formatOptions();
        }
        // Otherwise, get a wowhead url that automaticlly will be picked up
        // by the javascript code and render a tooltip.
        else {
            $attr['href'] = $this->renderUrl();
            $attr['target'] = '_blank';
        }

        return view("components.wowhead-link", [
            'component_attr' => $attr->filter()->toArray()
        ]);
    }

    /**
     * Render a wowhead url.
     */
    protected function renderUrl() : string
    {
        return sprintf('https://%s.wowhead.com/%s=%s',
            $this->domain, $this->type, $this->id);
    }

    /**
     * Format options for 'data-wowhead' attribute.
     */
    protected function formatOptions() : string
    {
        return http_build_query([
            $this->type => $this->id,
            'domain' => $this->domain
        ]);
    }
}
