<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\Component;

abstract class Icon extends Component
{
    /**
     * Path prefix.
     */
    protected string $prefix = '';

    /**
     * Image url.
     */
    public string $url;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $name)
    {
        $file = Str::of($name)->lower()->append('.jpg');
        if (strlen($this->prefix)) {
            $file = collect([$this->prefix, $file])->join('/');
        }

        $this->url = Storage::disk('images')->url($file);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.image-icon');
    }
}
