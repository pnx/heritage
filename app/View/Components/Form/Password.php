<?php

namespace App\View\Components\Form;

class Password extends Input
{
    public function __construct($name = 'password', ?string $id = null, ?string $value = null, bool $disabled = false)
    {
        parent::__construct($name, $id, 'password', $value, $disabled);
    }
}
