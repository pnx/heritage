<?php

namespace App\View\Components\Form;

class Checkbox extends Input
{
    public function __construct($name, ?string $id = null, ?string $value = null, bool $disabled = false)
    {
        parent::__construct($name, $id, 'checkbox', $value, $disabled);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.inputs.checkbox');
    }
}
