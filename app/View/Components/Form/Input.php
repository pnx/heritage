<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Input extends Component
{
    public string $type;

    public ?string $id;

    public string $name;

    public ?string $value;

    public string $disabled;

    public function __construct($name, ?string $id = null, string $type = 'text', ?string $value = null, bool $disabled = false)
    {
        $this->id = $id;
        $this->type = $type;
        $this->name = $name;
        $this->value = old($name, $value);
        $this->disabled = $disabled ? 'disabled=disabled' : '';
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.inputs.input');
    }
}
