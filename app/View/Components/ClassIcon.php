<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ClassIcon extends Icon
{
    protected string $prefix = 'classes';
}
