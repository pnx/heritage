<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Session;
use Illuminate\View\Component;

class Notifications extends Component
{
    /**
     * Types of messages.
     *
     * @var array
     */
    protected $types = [ 'info', 'success', 'warning', 'error' ];

    /**
     * Position (left,center or right)
     *
     * @var string
     */
    public string $position = 'center';

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        $messages = [];

        foreach($this->types as $type) {

            if (Session::has($type)) {
                $messages[$type] = Session::get($type);
            }
        }

        return view('components.notifications', [ 'messages' => $messages ]);
    }
}
