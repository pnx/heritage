<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Layout extends Component
{
    /**
     * Layout script
     */
    protected string $name;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name = 'default')
    {
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view("layouts.{$this->name}");
    }
}
