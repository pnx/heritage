<?php

namespace App\View\Components;

use Illuminate\View\Component;

/**
 * Render wowhead tooltip javascript
 */
class WowheadScript extends Component
{
    public $options;

    /**
     * Option keys.
     */
    protected static $keys = [
        'iconSize',
        'colorLinks',
        'iconizeLinks',
        'renameLinks',
        'hide'
    ];

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $this->options = collect(config('wowhead'))
            ->only(self::$keys)
            ->filter(function ($value, $key) {
                return is_array($value) ? count($value) : $value !== null;
            });

        return <<<'html'
        <script>const whTooltips=<?php echo json_encode($options) ?>;</script>
        <script src="https://wow.zamimg.com/widgets/power.js"></script>
        html;
    }
}
