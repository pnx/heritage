<?php

namespace App\View\Components;

class ProfessionIcon extends Icon
{
    protected string $prefix = 'professions';
}
