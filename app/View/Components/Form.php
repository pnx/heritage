<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{
    public string $method;

    public $spoofMethod = false;

    public function __construct($method = 'POST')
    {
        if (in_array($method, ['DELETE', 'PATCH'])) {
            $this->spoofMethod = $method;
            $method = 'POST';
        }

        $this->method = $method;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.form');
    }
}
