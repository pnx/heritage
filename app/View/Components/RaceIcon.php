<?php

namespace App\View\Components;

use App\Models\Character;
use Illuminate\Support\Str;

class RaceIcon extends Icon
{
    protected string $prefix = 'races';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Character $character)
    {
        $race = Str::of($character->race)->lower()->replace(' ', '');
        $name = sprintf("%s_%s", $race, $character->gender);

        parent::__construct($name);
    }
}
