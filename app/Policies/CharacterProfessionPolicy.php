<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Character;
use App\Models\CharacterProfession;

use Illuminate\Auth\Access\HandlesAuthorization;

class CharacterProfessionPolicy
{
    use HandlesAuthorization;

    public function delete(User $user, CharacterProfession $ch_prof)
    {
        return $user->id === $ch_prof->character->user_id;
    }
}
