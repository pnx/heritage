<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Character;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CharacterPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        $limit = 8;

        if ($user->characters->count() < $limit) {
            return Response::allow();
        }

        $message = __('authorization.character.limit', [ 'limit' => $limit ]);
        return Response::deny($message);
    }

    public function update(User $user, Character $character)
    {
        return $user->id === $character->user_id;
    }

    public function delete(User $user, Character $character)
    {
        return $user->id === $character->user_id;
    }

    /**
     * Check if use can import profession for this character.
     */
    public function import_profession(User $user, Character $character)
    {
        return $user->id === $character->user_id;
    }
}
