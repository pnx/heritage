<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Character;
use App\Models\Profession;
use App\Models\CharacterProfession;
use App\Models\Recipe;
use App\Models\RecipeCategory;
use App\Models\Item;
use App\Models\Spell;
use App\ProfessionImport\InvalidCharacterException;
use App\ProfessionImport\InvalidProfessionException;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

use Exception;

class ImportProfession implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Character profession the operation should act upon,
     */
    protected CharacterProfession $ch_prof;

    /**
     * Array of recipe data to the imported.
     */
    protected array $recipes;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, User $user, ?Character $character = null)
    {
        // Validate character name first.
        if ($character) {
            if ($character->name !== $data->player) {
                $message = sprintf('Wrong character: %s expected %s', $data->player, $character->name);
                throw new InvalidCharacterException($message);
            }
        } else {
            $character = Character::where('name', $data->player)
                ->where('user_id', $user->id)->first();

            if (!$character) {
                $message = sprintf('Could not find character "%s"', $data->player);
                throw new InvalidCharacterException($message);
            }
        }

        // Validate profession
        $profession = Profession::slug($data->profession->name)->first();
        if (!$profession) {
            $message = sprintf('Invalid profession: %s', $data->profession->name);
            throw new InvalidProfessionException($message);
        }

        // Find/Create specialization
        if (isset($data->profession->specializationId) && $data->profession->specializationId) {
            $specialization_id = Spell::firstOrCreate(
                ['id' => $data->profession->specializationId],
                ['name' => $data->profession->specializationName]
            )->id;
        } else {
            $specialization_id = null;
        }

        // Create/update profession for player.
        $this->ch_prof = $character->professions()->updateOrCreate(['profession_id' => $profession->id], [
            'specialization_id' => $specialization_id,
            'skill' => $data->profession->level,
        ]);

        $this->recipes = $data->profession->recipes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::transaction(function () {

            $this->processItems();
            $this->processSpells();

            $profession = $this->ch_prof->profession;

            $recipes = [];

            // Create recipes for character
            foreach($this->recipes as $data) {

                // Find spell
                if (isset($data->spellId)) {
                    $spell = Spell::find($data->spellId);
                    if (!$spell) {
                        throw new Exception("Could not find spell '{$data->spellId}' - '{$data->name}'");
                    }
                } else {
                    $spell = Spell::where('name', $data->name)->first();
                    if (!$spell) {
                        throw new Exception("Could not find spell '{$data->name}'");
                    }
                }

                // Find crafted item (if any)
                $crafted = Item::where('name', $data->name)->first();

                $recipes[] = $this->getRecipe($spell, $crafted, $profession, $data)->id;
            }

            // Update attached recipes.
            $this->ch_prof->recipes()->sync($recipes);
        });
    }

    protected function processSpells()
    {
        $spells = collect($this->recipes);
        $spells = $spells->except('items')
            ->map(function ($item, $key) {

                if (!isset($item->spellId)) {
                    return [];
                }

                return [
                    'id' => $item->spellId,
                    'name' => $item->name,
                    'slug' => Str::slug($item->name),
                ];
            })
            ->filter()
            ->sortBy('name')
            ->toArray();

        Spell::insertOrIgnore($spells, [ 'id', 'name', 'slug']);
    }

    protected function processItems()
    {
        $items = collect($this->recipes);
        $nested = $items->pluck('items')->flatten()->unique('name');
        $items = $items->except('items')
            ->merge($nested)
            ->map(function ($item, $key) {

                // 2021-07-01: Profession Exporter v1.1.0 is abit buggy and dont return
                // names for reagents. so we skip those.

                // 2021-07-08: Also skip items without id.
                if (!isset($item->name) || !isset($item->id)) {
                    return [];
                }

                return [
                    'name' => $item->name,
                    'slug' => Str::slug($item->name),
                    'external_id' => $item->id,
                    'texture' => isset($item->texture) ? $item->texture : null,
                    'color' => isset($item->color) ? (string) Str::of($item->color)->replace('#', '')->limit(8) : null,
                ];
            })
            ->filter()
            ->sortBy('name')
            ->toArray();

        Item::upsert($items, [ 'name', 'slug' ], [ 'external_id', 'texture', 'color' ]);
    }

    protected function getRecipe(Spell $spell, ?Item $crafted, Profession $profession, $data)
    {
        // Find by spell_id or fallback to craft->name if null.
        $recipe = $profession->recipes()
            ->where('spell_id', $spell->id)
            ->orWhereHas('craft', function($q) use ($data) {
                $q->where('name', $data->name);
            })
            ->first();

        // Create if not found.
        if ($recipe === null) {

            $category = RecipeCategory::firstOrCreate([ 'name' => $data->categorie ]);

            $recipe = $profession->recipes()->create([
                'item_id'       => $crafted ? $crafted->id : null,
                'spell_id'      => $spell->id,
                'category_id'   => $category->id,
                'description'   => isset($data->description) ? $data->description : null
            ]);
        }
        // Existing record.
        else {
            // Update with crafted item :)
            if (!$recipe->craft && $crafted) {
                $recipe->craft()->associate($crafted);
            }

            // Update spell if it dont exist.
            if (!$recipe->spell) {
                $recipe->spell()->associate($spell);
            }

            if ($recipe->description === NULL && isset($data->description)) {
                $recipe->description = $data->description;
            }
        }

        // Insert/Update Reagents
        $reagents = [];
        foreach($data->items as $reagent) {

            // 2021-07-01: Profession Exporter v1.1.0 is abit buggy and dont return
            // names for reagents. So we try to find item by id in that case.
            if (isset($reagent->name)) {
                $item = Item::where('name', $reagent->name)->first();
            } else {
                $item = Item::where('external_id', $reagent->id)->first();
            }

            if (!$item) {
                if (isset($reagent->name)) {
                    throw new Exception("Could not find item '{$reagent->name}'");
                }
                throw new Exception("Could not find item with id '{$reagent->id}'");
            }

            $reagents[$item->id] = [ 'quantity' => $reagent->num ];
        }

        $recipe->reagents()->sync($reagents);
        $recipe->push();

        return $recipe;
    }
}
