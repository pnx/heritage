const colors = require('tailwindcss/colors')

module.exports = {
	purge: {
		content: [
			'./resources/**/*.blade.php',
			'./resources/**/*.js',
			'./resources/**/*.vue',
		],
		options: {
			safelist: {
				standard: [ /-class-/ ],
			},
		},
	},
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			colors: {
				'class-druid': '#FF7C0A',
				'class-hunter': '#AAD372',
				'class-mage': '#3FC7EB',
				'class-paladin': '#F48CBA',
				'class-priest': '#c9c9c9',
				'class-rogue': '#FFF468',
				'class-shaman': '#0070DD',
				'class-warlock': '#8788EE',
				'class-warrior': '#C69B6D',
			},
		},
		scale: {
			'0': '0',
			'25': '.25',
			'50': '.5',
			'75': '.75',
			'90': '.9',
			'95': '.95',
			'100': '1',
			'105': '1.05',
			'110': '1.1',
			'125': '1.25',
			'150': '1.5',
			'175': '1.75',
			'200': '2',
		},
	},
	variants: {
		extend: {},
	},
	plugins: [
		require('@tailwindcss/forms'),
	],
}
