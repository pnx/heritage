<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_page_can_be_viewed()
    {
        $user = User::factory()->create();

        $response = $this->get(route('user.show', [ 'user' => $user ]));

        $response->assertStatus(200);
    }
}
