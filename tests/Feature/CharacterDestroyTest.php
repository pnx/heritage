<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Character;

class CharacterDestroyTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_delete_their_own_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()->for($user)->create();

        $response = $this->actingAs($user)
            ->delete(route('character.destroy', [ 'character' => $character ]));

        $response->assertStatus(302); // Redirect

        $this->assertDatabaseMissing('characters', [ 'id' => $character->id, 'deleted_at' => NULL ]);
    }

    public function test_deleting_main_character_sets_character_id_to_null_on_user()
    {
        $user = User::factory()->create();
        $character = Character::factory()->for($user)->create();

        $user->character_id = $character->id;
        $user->save();

        $character->delete();

        $this->assertDatabaseHas('users', [ 'id' => $user->id, 'character_id' => NULL ]);
        $this->assertDatabaseMissing('characters', [ 'id' => $character->id, 'deleted_at' => NULL ]);
    }

    public function test_guest_can_not_delete_character()
    {
        $character = Character::factory()->create();

        $response = $this->delete(route('character.destroy', [ 'character' => $character ]));

        $response->assertStatus(403); // Not allowed

        $this->assertDatabaseHas('characters', [ 'id' => $character->id, 'deleted_at' => NULL ]);
    }

    public function test_user_can_not_delete_someone_elses_character()
    {
        $user = User::factory()->create();
        $userOther = User::factory()->create();
        $character = Character::factory()->for($userOther)->create();

        $response = $this->actingAs($user)
            ->delete(route('character.destroy', [ 'character' => $character ]));

        $response->assertStatus(403); // Not allowed

        $this->assertDatabaseHas('characters', [ 'id' => $character->id, 'deleted_at' => NULL ]);
    }
}
