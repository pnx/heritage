<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Recipe;
use App\Models\Item;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test recipes relationship.
     *
     * @return void
     */
    public function test_recipes_relationship()
    {
        $item = Item::factory()->create();

        $recipe = Recipe::factory()
            ->for($item, 'craft')
            ->create();

        $recipe2 = Recipe::factory()
            ->for($item, 'craft')
            ->create();

        $this->assertEquals($recipe->id, $item->recipes[0]->id);
        $this->assertEquals($recipe2->id, $item->recipes[1]->id);
    }

    /**
     * Test reagent for relationship.
     *
     * @return void
     */
    public function test_reagent_for_relationship()
    {
        $item = Item::factory()->create();

        $recipe = Recipe::factory()
            ->hasAttached($item, ['quantity' => 1], 'reagents')
            ->create();

        $recipe2 = Recipe::factory()
            ->hasAttached($item, ['quantity' => 1], 'reagents')
            ->create();

        $this->assertEquals($recipe->id, $item->reagent_for[0]->id);
        $this->assertEquals($recipe2->id, $item->reagent_for[1]->id);
    }

    /**
     * Test quantity attribute
     *
     * @return void
     */
    public function test_quantity_attribute()
    {
        $recipe = Recipe::factory()->create();
        $item = Item::factory()->create();
        $recipe->reagents()->attach($item, ['quantity' => 5]);

        $this->assertEquals(5, $recipe->reagents[0]->quantity);
    }

    /**
     * Test quantity attribute missing
     *
     * @return void
     */
    public function test_quantity_attribute_missing()
    {
        $item = Item::factory()->create();

        $this->assertNull($item->quantity);
    }
}
