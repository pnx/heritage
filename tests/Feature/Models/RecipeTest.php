<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Recipe;
use App\Models\RecipeCategory;
use App\Models\Profession;
use App\Models\CharacterProfession;
use App\Models\Character;
use App\Models\Spell;
use App\Models\Item;

class RecipeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test profession relationship.
     *
     * @return void
     */
    public function test_profession_relationship()
    {
        $profession = Profession::factory()->create();

        $recipe = Recipe::factory()
            ->for($profession)
            ->create();

        $this->assertEquals($profession->id, $recipe->profession->id);
    }

    /**
     * Test category relationship.
     *
     * @return void
     */
    public function test_category_relationship()
    {
        $category = RecipeCategory::factory()->create();

        $recipe = Recipe::factory()
            ->for($category, 'category')
            ->create();

        $this->assertEquals($category->id, $recipe->category->id);
    }

    /**
     * Test spell relationship.
     *
     * @return void
     */
    public function test_spell_relationship()
    {
        $spell = Spell::factory()->create();

        $recipe = Recipe::factory()
            ->for($spell)
            ->create();

        $this->assertEquals($spell->id, $recipe->spell->id);
    }

    /**
     * Test craft relationship.
     *
     * @return void
     */
    public function test_craft_relationship()
    {
        $item = Item::factory()->create();

        $recipe = Recipe::factory()
            ->for($item, 'craft')
            ->create();

        $this->assertEquals($item->id, $recipe->craft->id);
    }

    /**
     * Test character profession relationship.
     *
     * @return void
     */
    public function test_character_profession_relationship()
    {
        $profession = Profession::factory()->create();
        $ch_profs = CharacterProfession::factory(2)->create([ 'profession_id' => $profession->id ]);
        $characters = Character::factory(2)->create();

        $characters[0]->professions()->save($ch_profs[0]);
        $characters[1]->professions()->save($ch_profs[1]);

        $recipe = Recipe::factory()
            ->for($profession)
            ->create();

        $characters[0]->professions[0]->recipes()->save($recipe);
        $characters[1]->professions[0]->recipes()->save($recipe);

        $professions = $recipe->character_profession;

        $this->assertEquals($ch_profs[0]->id, $professions[0]->id);
        $this->assertEquals($ch_profs[1]->id, $professions[1]->id);
    }

    /**
     * Test crafters relationship.
     *
     * @return void
     */
    public function test_crafters_relationship()
    {
        $profession = Profession::factory()->create();
        $ch_profs = CharacterProfession::factory(2)->create([ 'profession_id' => $profession->id ]);
        $characters = Character::factory(2)->create();

        $characters[0]->professions()->save($ch_profs[0]);
        $characters[1]->professions()->save($ch_profs[1]);

        $recipe = Recipe::factory()
            ->for($profession)
            ->create();

        // Make both character learn the recipe.
        $characters[0]->professions[0]->learn($recipe);
        $characters[1]->professions[0]->learn($recipe);

        $crafters = $recipe->crafters;

        $this->assertEquals($crafters[0]->id, $characters[0]->id);
        $this->assertEquals($crafters[1]->id, $characters[1]->id);
    }

    /**
     * Test reagants relationship.
     *
     * @return void
     */
    public function test_reagents_relationship()
    {
        $profession = Profession::factory()->create();
        $items = Item::factory(3)->create();

        $recipe = Recipe::factory()
            ->for($profession)
            ->create();

        $recipe->reagents()->attach($items[0], ['quantity' => 1]);
        $recipe->reagents()->attach($items[1], ['quantity' => 4]);
        $recipe->reagents()->attach($items[2], ['quantity' => 10]);

        $this->assertEquals($items[0]->id, $recipe->reagents[0]->id);
        $this->assertEquals($items[1]->id, $recipe->reagents[1]->id);
        $this->assertEquals($items[2]->id, $recipe->reagents[2]->id);
    }
}
