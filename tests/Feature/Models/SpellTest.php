<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\Recipe;
use App\Models\Spell;

class SpellTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test recipe relationship.
     *
     * @return void
     */
    public function test_recipe_relationship()
    {
        $spell = Spell::factory()->create();
        $recipe = Recipe::factory()
            ->for($spell)
            ->create();

        $this->assertEquals($recipe->name, $spell->recipe->name);
    }
}
