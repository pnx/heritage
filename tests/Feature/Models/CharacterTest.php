<?php

namespace Tests\Feature\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use App\Models\User;
use App\Models\CharacterProfession;
use App\Models\Character;

class CharacterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test user relationship.
     *
     * @return void
     */
    public function test_user_relationship()
    {
        $user = User::factory()->create();
        $character = Character::factory()
            ->for($user)
            ->create();

        $this->assertEquals($user->id, $character->user->id);
    }

    /**
     * Test character profession relationship.
     *
     * @return void
     */
    public function test_profession_relationship()
    {
        $this->seed(\Database\Seeders\ProductionSeeders\ProfessionsTableSeeder::class);

        $professions = CharacterProfession::factory(2)->create();
        $character = Character::factory()->create();

        $character->professions()->save($professions[0]);
        $character->professions()->save($professions[1]);

        $this->assertEquals(2, $character->professions->count());

        foreach($professions as $profession) {
            $this->assertContains($profession->id, $character->professions->pluck('id')->toArray());
        }
    }
}
