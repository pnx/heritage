<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Character;
use App\Models\CharacterProfession;
use App\Models\Profession;

class CharacterProfessionTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_is_allowed_to_create_profession_for_their_own_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()
            ->for($user)
            ->create();

        $response = $this->actingAs($user)
            ->get(route('character.profession.create', [ 'character' => $character ]));

        $response->assertStatus(200); // OK
    }

    public function test_user_is_not_allowed_to_create_profession_for_someone_elses_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('character.profession.create', [ 'character' => $character ]));

        $response->assertStatus(403); // Not allowed
    }

    public function test_user_is_allowed_to_delete_profession_from_their_own_character()
    {
        $user = User::factory()->create();
        $profession = Profession::factory()->create();
        $character = Character::factory()
            ->for($user)
            ->create();

        $ch_prof = CharacterProfession::factory()
            ->for($character)
            ->for($profession)
            ->create();

        $response = $this->actingAs($user)
            ->delete(route('character.profession.destroy', [ 'character' => $character, 'profession' => $profession ]));

        // The response should be a redirect in this case.
        $response->assertStatus(302);

        // Check that the character profession is deleted.
        $this->assertDatabaseMissing('character_professions', [ 'id' => $ch_prof->id ]);
    }

    public function test_user_is_not_allowed_to_delete_profession_from_someone_elses_character()
    {
        $user = User::factory()->create();
        $profession = Profession::factory()->create();
        $character = Character::factory()->create();

        $ch_prof = CharacterProfession::factory()
            ->for($character)
            ->for($profession)
            ->create();

        $response = $this->actingAs($user)
            ->delete(route('character.profession.destroy', [ 'character' => $character, 'profession' => $profession ]));

        $response->assertStatus(403);  // Not allowed

        // Check that the character profession is still there.
        $this->assertDatabaseHas('character_professions', [ 'id' => $ch_prof->id ]);
    }
}
