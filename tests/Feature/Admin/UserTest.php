<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use App\Http\Livewire\Form\Admin\UserForm;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function test_admin_can_view_users_list()
    {
        $user = User::factory()->create(['role' => 'admin']);

        $response = $this->actingAs($user)
            ->get(route('admin.users.index'));

        $response->assertStatus(200);
    }

    public function test_non_admin_cannot_view_users_list()
    {
        $user = User::factory()->create(['role' => 'user']);

        // Standard user
        $response = $this->actingAs($user)
            ->get(route('admin.users.index'));

        $response->assertForbidden("Standard user");

        // Guest
        $response = $this->get(route('admin.users.index'));

        $response->assertForbidden("Guest");
    }

    public function test_admin_can_render_create_page()
    {
        $user = User::factory()->create(['role' => 'admin']);

        $response = $this->actingAs($user)
            ->get(route('admin.users.create'));

        $response->assertStatus(200);
    }

    public function test_non_admin_cannot_render_create_page()
    {
        $user = User::factory()->create(['role' => 'user']);

        // Standard user
        $response = $this->actingAs($user)
            ->get(route('admin.users.create'));

        $response->assertForbidden("Standard user");

        // Guest
        $response = $this->get(route('admin.users.create'));

        $response->assertForbidden("Guest");
    }

    public function test_admin_can_create_user()
    {
        $user = User::factory()->create(['role' => 'admin']);

        $this->actingAs($user);

        \Livewire::test(UserForm::class)
            ->set('user.username', 'scammer123')
            ->set('user.role', 'user')
            ->set('password', 'password1234')
            ->set('password_confirmation', 'password1234')
            ->call('save')
            ->assertRedirect(route('admin.users.index'));

        $this->assertDatabaseHas('users', [
            'username' => 'scammer123',
            'role' => 'user'
        ]);
    }

    public function test_non_admin_cannot_create_users()
    {
        // Guest
        \Livewire::test(UserForm::class)
            ->set('user.username', 'nonadmin')
            ->set('user.role', 'user')
            ->set('password', 'password1234')
            ->set('password_confirmation', 'password1234')
            ->call('save')
            ->assertForbidden();

        $this->assertDatabaseMissing('users', ['username' => 'nonadmin']);

        // Standard user
        $this->actingAs(User::factory()->create(['role' => 'user']));

        \Livewire::test(UserForm::class)
            ->set('user.username', 'nonadmin')
            ->set('user.role', 'user')
            ->set('password', 'password1234')
            ->set('password_confirmation', 'password1234')
            ->call('save')
            ->assertForbidden();

        $this->assertDatabaseMissing('users', ['username' => 'nonadmin']);
    }

    public function test_admin_can_render_edit_page()
    {
        $user = User::factory()->create(['role' => 'admin']);
        $edit = User::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('admin.users.edit', ['user' => $edit]));

        $response->assertStatus(200);
    }

    public function test_non_admin_cannot_render_edit_page()
    {
        $user = User::factory()->create(['role' => 'user']);
        $edit = User::factory()->create();

        // Standard user
        $response = $this->actingAs($user)
            ->get(route('admin.users.edit', ['user' => $edit]));

        $response->assertForbidden("Standard user");

        // Guest
        $response = $this->get(route('admin.users.edit', ['user' => $edit]));

        $response->assertForbidden("Guest");
    }

    public function test_admin_can_edit_user()
    {
        $user = User::factory()->create(['role' => 'admin']);
        $edit = User::factory()->create(['role' => 'user']);

        $this->actingAs($user);

        \Livewire::test(UserForm::class, [ 'user' => $edit ])
            ->set('user.username', 'Edited')
            ->call('save')
            ->assertRedirect(route('admin.users.index'));

        $this->assertDatabaseHas('users', [
            'username' => 'Edited',
            'role' => 'user',
        ]);
    }

    public function test_non_admin_cannot_edit_user()
    {
        $edit = User::factory()->create(['username' => 'Untouched', 'role' => 'user']);

        // Guest
        \Livewire::test(UserForm::class, [ 'user' => $edit ])
            ->set('user.username', 'Cantedit')
            ->call('save')
            ->assertForbidden();

        $this->assertDatabaseHas('users', [
            'username' => 'Untouched',
            'role' => 'user',
        ]);

        // Standard user
        $this->actingAs(User::factory()->create(['role' => 'user']));

        \Livewire::test(UserForm::class, [ 'user' => $edit ])
            ->set('user.username', 'Cantedit')
            ->call('save')
            ->assertForbidden();

        $this->assertDatabaseHas('users', [
            'username' => 'Untouched',
            'role' => 'user',
        ]);
    }

    public function test_admin_can_delete_user()
    {
        $user = User::factory()->create(['role' => 'admin']);
        $delete = User::factory()->create();

        $response = $this->actingAs($user)
            ->delete(route('admin.users.destroy', [ 'user' => $delete ]));

        $this->assertDatabaseMissing('users', [ 'id' => $delete->id, 'deleted_at' => NULL ]);
    }

    public function test_non_admin_cannot_delete_user()
    {
        $user = User::factory()->create(['role' => 'user']);
        $delete = User::factory()->create();

        // Standard user
        $response = $this->actingAs($user)
            ->delete(route('admin.users.destroy', [ 'user' => $delete ]));

        $response->assertForbidden("Standard user");
        $this->assertDatabaseHas('users', [ 'id' => $delete->id, 'deleted_at' => NULL ]);

        // Guest
        $response = $this->delete(route('admin.users.destroy', [ 'user' => $delete ]));

        $response->assertForbidden("Guest");
        $this->assertDatabaseHas('users', [ 'id' => $delete->id, 'deleted_at' => NULL ]);
    }
}
