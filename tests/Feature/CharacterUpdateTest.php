<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Character;

use App\Http\Livewire\Form\CharacterForm;

class CharacterUpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_render_character_edit_page()
    {
        $user = User::factory()->create();
        $character = Character::factory()->for($user)->create();

        $response = $this->actingAs($user)
            ->get(route('character.edit', ['character' => $character]));

        $response->assertStatus(200); // OK
    }

    public function test_guest_can_not_render_character_edit_page_for_someone_elses_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('character.edit', ['character' => $character]));

        $response->assertStatus(403); // Forbidden
    }

    public function test_user_can_update_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()->for($user)->create([
            'name' => 'Mychar',
            'level' => '60',
            'class' => 'warrior',
            'race' => 'human',
            'gender' => 'M'
        ]);

        $this->actingAs($user);

        \Livewire::test(CharacterForm::class, ['character' => $character])
            ->set('character.name', 'Updated')
            ->set('character.level', '70')
            ->set('character.class', 'priest')
            ->set('character.race', 'dwarf')
            ->set('character.gender', 'F')
            ->call('save')
            ->assertRedirect(route('profile.index'));

        // Check database that character was updated.
        $this->assertDatabaseHas('characters', [
            'name' => 'Updated',
            'level' => '70',
            'race' => 'dwarf',
            'class' => 'priest',
            'gender' => 'F'
        ]);
    }

    public function test_user_can_not_update_someone_elses_character()
    {
        $user = User::factory()->create();
        $character = Character::factory()->create([
            'race' => 'undead',
            'class' => 'rogue'
        ]);;

        $this->actingAs($user);

        // Try update via livewire form.
        \Livewire::test(CharacterForm::class, ['character' => $character])
            ->set('character.name', 'Mynewname')
            ->call('save')
            ->assertForbidden();

        // Check database that character has old data
        $this->assertDatabaseHas('characters', [
            'name' => $character->name,
            'level' => $character->level,
            'race' => $character->race,
            'class' => $character->class,
            'gender' => $character->gender
        ]);
    }

    public function test_guest_can_not_update_characters()
    {
        $character = Character::factory()->create([
            'race' => 'troll',
            'class' => 'mage'
        ]);

        \Livewire::test(CharacterForm::class, ['character' => $character])
            ->set('character.name', 'Guestchar')
            ->set('character.level', '61')
            ->set('character.class', 'priest')
            ->set('character.race', 'dwarf')
            ->set('character.gender', 'M')
            ->call('save')
            ->assertForbidden();

        // Check database that character has old data
        $this->assertDatabaseHas('characters', [
            'name' => $character->name,
            'level' => $character->level,
            'race' => $character->race,
            'class' => $character->class,
            'gender' => $character->gender
        ]);
    }
}
