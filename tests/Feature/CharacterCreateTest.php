<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Character;

use App\Http\Livewire\Form\CharacterForm;

class CharacterCreateTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_render_character_creation_page()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('character.create'));

        $response->assertStatus(200); // OK
    }

    public function test_guest_can_not_render_character_creation_page()
    {
        $response = $this->get(route('character.create'));

        $response->assertStatus(403); // Forbidden
    }

    public function test_user_can_create_characters()
    {
        $this->actingAs(User::factory()->create());

        \Livewire::test(CharacterForm::class)
            ->set('character.name', 'Elise')
            ->set('character.level', '70')
            ->set('character.class', 'warrior')
            ->set('character.race', 'human')
            ->set('character.gender', 'F')
            ->call('save')
            ->assertRedirect(route('profile.index'));

        // Find character and check the data.
        $character = Character::where('name', 'Elise')->first();

        $this->assertEquals(70, $character->level);
        $this->assertEquals('warrior', $character->class);
        $this->assertEquals('human', $character->race);
        $this->assertEquals('F', $character->gender);
    }

    public function test_user_can_not_create_alot_of_characters()
    {
        $user = User::factory()->create();

        // Create "alot" of characters (8 or so is the limit in the front-end).
        Character::factory()
            ->for($user)
            ->count(20)->create();

        $this->actingAs($user);

        // Try create one more via livewire form.
        \Livewire::test(CharacterForm::class)
            ->set('character.name', 'Notonemore')
            ->set('character.level', '10')
            ->set('character.class', 'mage')
            ->set('character.race', 'troll')
            ->set('character.gender', 'M')
            ->call('save')
            ->assertForbidden();
    }

    public function test_guest_can_not_create_characters()
    {
        \Livewire::test(CharacterForm::class)
            ->set('character.name', 'Guestchar')
            ->set('character.level', '61')
            ->set('character.class', 'priest')
            ->set('character.race', 'dwarf')
            ->set('character.gender', 'M')
            ->call('save')
            ->assertForbidden();

        $this->assertDatabaseMissing('characters', [ 'name' => 'Guestchar' ]);
    }
}
