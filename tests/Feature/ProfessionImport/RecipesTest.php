<?php

namespace Tests\Feature\ProfessionImport;

use App\Models\User;
use App\Models\Character;
use App\Models\Profession;
use App\Models\Item;
use App\Models\Recipe;
use App\Jobs\ImportProfession;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RecipesTest extends TestCase
{
    use RefreshDatabase;

    public function test_import_recipe_without_craft_item()
    {
        $this->seed(\Database\Seeders\ProductionSeeders\ItemSeeder::class);

        $data = (object) [
            "server" => "Whitemane",
            "player" => "Scumbag",
            "guild" => "Method",
            "profession" => (object) [
                "locale" => "enUS",
                "name" => "Enchanting",
                "specializationId" => null,
                "specializationName" => null,
                "level" => 303,
                "maxLevel" => 375,
                "recipes" => [
                    (object) [
                        "description" => "Permanently enchant boots to give +7 Agility.",
                        "name" => "Enchant Boots - Greater Agility",
                        "num" => 0,
                        "categorie" => "none",
                        "items" => [
                            (object) [
                                "color" => "1eff00",
                                "id" => 16203,
                                "num" => 8
                            ]
                        ],
                        "spellId" => 20023
                    ],
                    (object) [
                        "description" => "Transforms a Nexus Crystal into a Small Prismatic Shard.",
                        "name" => "Nexus Transformation",
                        "num" => 1,
                        "categorie" => "none",
                        "items" => [
                            (object) [
                                "color" => "a335ee",
                                "id" => 20725,
                                "num" => 1
                            ]
                        ],
                        "spellId" => 42613
                    ]
                ]
            ]
        ];

        Profession::factory()->create([ 'name' => 'Enchanting' ]);
        $character = Character::factory()->create([ 'name' => 'Scumbag']);

        ImportProfession::dispatch($data, $character->user, $character);

        $this->assertEquals('Enchanting', $character->professions[0]->name);
        $recipes = $character->professions[0]->recipes;

        // Recipe 1
        $this->assertNull($recipes[0]->craft);

        $this->assertEquals(20023, $recipes[0]->spell->id);
        $this->assertEquals('Enchant Boots - Greater Agility', $recipes[0]->spell->name);

        $this->assertEquals(16203, $recipes[0]->reagents[0]->external_id);
        $this->assertEquals('Greater Eternal Essence', $recipes[0]->reagents[0]->name);
        $this->assertEquals(8, $recipes[0]->reagents[0]->quantity);

        // Recipe 2
        $this->assertNull($recipes[1]->craft);

        $this->assertEquals(42613, $recipes[1]->spell->id);
        $this->assertEquals('Nexus Transformation', $recipes[1]->spell->name);

        $this->assertEquals(20725, $recipes[1]->reagents[0]->external_id);
        $this->assertEquals('Nexus Crystal', $recipes[1]->reagents[0]->name);
        $this->assertEquals(1, $recipes[1]->reagents[0]->quantity);
    }

    public function test_import_does_not_create_duplicate_recipes_if_spell_relationship_is_missing()
    {
        $character = Character::factory()->create([ 'name' => 'Duplicated']);
        $profession = Profession::factory()->create([ 'name' => 'My Unique Profession' ]);
        $item = Item::factory()->create(['name' => 'Unique Recipe']);

        $recipe = Recipe::factory()
            ->for($profession)
            ->for($item, 'craft')
            ->create(['spell_id' => null]);

        $data = (object) [
            "server" => "Mograine",
            "player" => $character->name,
            "guild" => "Odyssey",
            "profession" => (object) [
                "name" => $profession->name,
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => [
                    (object) [
                        "name" => $item->name,
                        "num" => 0,
                        "categorie" => "none",
                        "items" => [],
                        "spellId" => 1337
                    ]
                ]
            ]
        ];

        ImportProfession::dispatch($data, $character->user, $character);

        $this->assertEquals(1, Recipe::count());
        $this->assertDatabaseHas('recipes', [
            'spell_id' => 1337,
            'item_id' => $item->id,
        ]);
    }
}
