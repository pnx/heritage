<?php

namespace Tests\Feature\ProfessionImport;

use App\Models\Character;
use App\Models\Profession;
use App\Models\Spell;
use App\Jobs\ImportProfession;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SpecializationTest extends TestCase
{
    use RefreshDatabase;

    public function test_specialization_is_imported_correctly()
    {
        $character = Character::factory()->create([ 'name' => 'Specialdude']);
        $profession = Profession::factory()->create([ 'name' => 'Alchemy' ]);
        $specialization = Spell::factory()->create(['id' => 28672, 'name' => 'Transmutation Master']);

        $data = (object) [
            "server" => "Firemaw",
            "player" => $character->name,
            "guild" => "Salad Bakers",
            "profession" => (object) [
                "name" => $profession->name,
                "specializationId" => 28672,
                "specializationName" => "Transmutation Master",
                "level" => 375,
                "recipes" => [],
            ]
        ];

        ImportProfession::dispatch($data, $character->user, $character);

        $this->assertNotNull($character->professions[0]->specialization, "specalization was not imported");
        $this->assertEquals(28672, $character->professions[0]->specialization->id);
        $this->assertEquals("Transmutation Master", $character->professions[0]->specialization->name);

        $this->assertEquals(1, Spell::count());
    }

    public function test_specialization_can_be_omitted()
    {
        $character = Character::factory()->create([ 'name' => 'frankthetank']);
        $profession = Profession::factory()->create([ 'name' => 'Carpenter' ]);

        $data = (object) [
            "server" => "Loatheb",
            "player" => $character->name,
            "guild" => "Progress",
            "profession" => (object) [
                "name" => $profession->name,
                "level" => 375,
                "recipes" => [],
            ]
        ];

        ImportProfession::dispatch($data, $character->user, $character);

        $this->assertNull($character->professions[0]->specialization);
    }
}
