<?php

namespace Tests\Feature\ProfessionImport;

use App\Models\User;
use App\Models\Character;
use App\Models\Profession;
use App\Jobs\ImportProfession;

use App\ProfessionImport\InvalidCharacterException;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CharacterTest extends TestCase
{
    use RefreshDatabase;

    public function test_character_found()
    {
        Profession::factory()->create([ 'name' => 'Alchemy' ]);
        $character = Character::factory()->create([ 'name' => 'Charfound']);

        $data = (object) [
            "server" => "Darkshore",
            "player" => "Charfound",
            "guild" => "ABOBA",
            "profession" => (object) [
                "name" => "Alchemy",
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => [
                    (object) [
                        "name" => "Elixir of Major Defense",
                        "color" => "ffffff",
                        "id" => 22834,
                        "num" => 1,
                        "categorie" => "Elixir",
                        "items" => [
                            (object) [
                                "name" => "Ancient Lichen",
                                "color" => "ffffff",
                                "num" => 3,
                                "id" => 22790
                            ],
                            (object) [
                                "name" => "Terocone",
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 22789
                            ],
                            (object) [
                                "name" => "Imbued Vial",
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 18256
                            ],
                        ],
                        "spellId" => 28557
                    ],
                ]
            ]
        ];

        ImportProfession::dispatch($data, $character->user);

        $this->assertEquals('Alchemy', $character->professions[0]->name);
        $recipes = $character->professions[0]->recipes;

        $this->assertEquals(22834, $recipes[0]->craft->external_id);
        $this->assertEquals('Elixir of Major Defense', $recipes[0]->craft->name);

        $this->assertEquals(28557, $recipes[0]->spell->id);
        $this->assertEquals('Elixir of Major Defense', $recipes[0]->spell->name);

        $this->assertEquals(22790, $recipes[0]->reagents[0]->external_id);
        $this->assertEquals('Ancient Lichen', $recipes[0]->reagents[0]->name);
        $this->assertEquals(3, $recipes[0]->reagents[0]->quantity);

        $this->assertEquals(18256, $recipes[0]->reagents[1]->external_id);
        $this->assertEquals('Imbued Vial', $recipes[0]->reagents[1]->name);
        $this->assertEquals(1, $recipes[0]->reagents[1]->quantity);

        $this->assertEquals(22789, $recipes[0]->reagents[2]->external_id);
        $this->assertEquals('Terocone', $recipes[0]->reagents[2]->name);
        $this->assertEquals(1, $recipes[0]->reagents[2]->quantity);
    }

    public function test_character_not_found()
    {
        $character = Character::factory()->create([ 'name' => 'Findme']);

        $data = (object) [
            "server" => "Ashbringer",
            "player" => "Anotherdude",
            "guild" => "Raiders of the Last Mark",
            "profession" => (object) [
                "name" => "Alchemy",
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => []
            ]
        ];

        $this->expectException(InvalidCharacterException::class);

        ImportProfession::dispatch($data, $character->user);
    }

    public function test_other_users_characters_is_not_found()
    {
        $character = Character::factory()->create([ 'name' => 'Otherchar']);

        $data = (object) [
            "server" => "Rattlegore",
            "player" => "Otherchar",
            "guild" => "The A-Team",
            "profession" => (object) [
                "name" => "Alchemy",
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => []
            ]
        ];

        $this->expectException(InvalidCharacterException::class);

        ImportProfession::dispatch($data, User::factory()->create());
    }

    public function test_character_is_not_the_same_as_imported_name()
    {
        $character = Character::factory()->create([ 'name' => 'Actualname']);

        $data = (object) [
            "server" => "Benediction",
            "player" => "Wrongname",
            "guild" => "Spaceballs",
            "profession" => (object) [
                "name" => "Alchemy",
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => []
            ]
        ];

        $this->expectException(InvalidCharacterException::class);

        ImportProfession::dispatch($data, $character->user, $character);
    }
}
