<?php

namespace Tests\Feature\ProfessionImport;

use App\Models\User;
use App\Models\Character;
use App\Models\CharacterProfession;
use App\Models\Profession;
use App\Models\Item;
use App\Models\Recipe;
use App\Models\Spell;
use App\Jobs\ImportProfession;

use App\ProfessionImport\InvalidProfessionException;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BasicTest extends TestCase
{
    use RefreshDatabase;

    public function test_import()
    {
        $this->seed(\Database\Seeders\ProductionSeeders\ItemSeeder::class);

        // Profession Exporter v1.1.0 format
        $data = (object) [
            "server" => "Ashbringer",
            "player" => "Superduper",
            "guild" => "Nej baba inte tofflan",
            "profession" => (object) [
                "locale" => "enUS",
                "name" => "Alchemy",
                "specializationId" => 28672,
                "specializationName" => "Transmutation Master",
                "level" => 374,
                "maxLevel" => 375,
                "recipes" => [
                    (object) [
                        "name" => "Elixir of Major Defense",
                        "color" => "ffffff",
                        "id" => 22834,
                        "num" => 1,
                        "categorie" => "Elixir",
                        "items" => [
                            (object) [
                                "color" => "ffffff",
                                "num" => 3,
                                "id" => 22790
                            ],
                            (object) [
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 22789
                            ],
                            (object) [
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 18256
                            ],
                        ],
                        "spellId" => 28557
                    ],
                    (object) [
                        "name" => "Elixir of Major Agility",
                        "color" => "ffffff",
                        "id" => 22831,
                        "num" => 1,
                        "categorie" => "Elixir",
                        "items" => [
                            (object) [
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 22789
                            ],
                            (object) [
                                "color" => "ffffff",
                                "num" => 2,
                                "id" => 22785
                            ],
                            (object) [
                                "color" => "ffffff",
                                "num" => 1,
                                "id" => 18256
                            ]
                        ],
                        "spellId" => 28553
                    ]
                ]
            ]
        ];

        Profession::factory()->create([ 'name' => 'Alchemy' ]);
        $character = Character::factory()->create([ 'name' => 'Superduper']);

        ImportProfession::dispatch($data, $character->user, $character);

        $this->assertEquals('Alchemy', $character->professions[0]->name);
        $recipes = $character->professions[0]->recipes;

        // Recipe 1
        $this->assertEquals(22834, $recipes[0]->craft->external_id);
        $this->assertEquals('Elixir of Major Defense', $recipes[0]->craft->name);

        $this->assertEquals(28557, $recipes[0]->spell->id);
        $this->assertEquals('Elixir of Major Defense', $recipes[0]->spell->name);

        $this->assertEquals(18256, $recipes[0]->reagents[0]->external_id);
        $this->assertEquals('Imbued Vial', $recipes[0]->reagents[0]->name);
        $this->assertEquals(1, $recipes[0]->reagents[0]->quantity);

        $this->assertEquals(22789, $recipes[0]->reagents[1]->external_id);
        $this->assertEquals('Terocone', $recipes[0]->reagents[1]->name);
        $this->assertEquals(1, $recipes[0]->reagents[1]->quantity);

        $this->assertEquals(22790, $recipes[0]->reagents[2]->external_id);
        $this->assertEquals('Ancient Lichen', $recipes[0]->reagents[2]->name);
        $this->assertEquals(3, $recipes[0]->reagents[2]->quantity);

        // Recipe 2
        $this->assertEquals(22831, $recipes[1]->craft->external_id);
        $this->assertEquals('Elixir of Major Agility', $recipes[1]->craft->name);

        $this->assertEquals(28553, $recipes[1]->spell->id);
        $this->assertEquals('Elixir of Major Agility', $recipes[1]->spell->name);

        $this->assertEquals(18256, $recipes[1]->reagents[0]->external_id);
        $this->assertEquals('Imbued Vial', $recipes[1]->reagents[0]->name);
        $this->assertEquals(1, $recipes[1]->reagents[0]->quantity);

        $this->assertEquals(22785, $recipes[1]->reagents[1]->external_id);
        $this->assertEquals('Felweed', $recipes[1]->reagents[1]->name);
        $this->assertEquals(2, $recipes[1]->reagents[1]->quantity);

        $this->assertEquals(22789, $recipes[1]->reagents[2]->external_id);
        $this->assertEquals('Terocone', $recipes[1]->reagents[2]->name);
        $this->assertEquals(1, $recipes[1]->reagents[2]->quantity);
    }

    public function test_invalid_profession()
    {
        Profession::factory()->create([ 'name' => 'Alchemy' ]);
        Profession::factory()->create([ 'name' => 'Tailoring' ]);
        Profession::factory()->create([ 'name' => 'Blacksmith' ]);
        $character = Character::factory()->create();

        $data = (object) [
            "server" => "Sulfuron",
            "player" => $character->name,
            "guild" => "Futuramalama",
            "profession" => (object) [
                "name" => "Carpenter",
                "level" => 375,
                "maxLevel" => 375,
                "recipes" => []
            ]
        ];

        $this->expectException(InvalidProfessionException::class);

        ImportProfession::dispatch($data, $character->user, $character);
    }
}
