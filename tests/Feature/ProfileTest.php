<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_profile()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get(route('profile.index'));

        $response->assertStatus(200);
    }

    public function test_guest_can_not_view_profile()
    {
        $response = $this->get(route('profile.index'));

        $response->assertRedirect(route('auth.login'));
    }

    public function test_user_can_update_username()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->post(route('profile.update'), [
                'username' => 'AnotherUsername',
                'current_password' => null,
                'password' => null,
                'password_confirmation' => null
            ]);

        $response->assertRedirect(route('profile.index'));

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'username' => 'AnotherUsername',
            'password' => $user->password
        ]);
    }

    public function test_user_can_update_password()
    {
        $fakeHash = '$2y$04$wiUTB.6ldFQ3TmxdSyizEubuKubDA45L/Bv0zlZ1.uoMcnm.ftIaK';

        $user = User::factory()->create();

        // Mock Hash::make() to return a fake password hash.
        Hash::shouldReceive('make')
            ->once()
            ->with('newpassword')
            ->andReturn($fakeHash);

        // Also have to mock Hash::check() as i can not get
        // partial mocks to work for facades.
        Hash::shouldReceive('check')
            ->with('password', $user->password)
            ->andReturn(true);

        $response = $this->actingAs($user)
            ->post(route('profile.update'), [
                'username' => $user->username,
                'current_password' => 'password',
                'password' => 'newpassword',
                'password_confirmation' => 'newpassword'
            ]);

        $response->assertRedirect(route('profile.index'));

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'username' => $user->username,
            'password' => $fakeHash
        ]);
    }

    public function test_user_can_not_set_empty_password_hash()
    {
        $user = User::factory()->create([ 'password' => NULL ]);

        $response = $this->actingAs($user)
            ->post(route('profile.update'), [
                'username' => $user->username,
                'current_password' => '',
                'password' => '',
                'password_confirmation' => ''
            ]);

        $response->assertRedirect(route('profile.index'));

        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'password' => NULL
        ]);
    }

    public function test_guest_can_not_update()
    {
        $response = $this->post(route('profile.update'), [
            'current_password' => 'password',
            'password' => 'newpassword',
            'password_confirmation' => 'newpassword'
        ]);

        $response->assertRedirect(route('auth.login'));
    }
}
