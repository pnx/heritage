<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Profession;
use App\Models\Character;
use App\Models\User;

class ProfessionTest extends TestCase
{
    use RefreshDatabase;

    public function test_guest_is_not_allowed_to_create_profession()
    {
        $response = $this->get(route('profession.create'));

        $response->assertRedirect(route('auth.login'));
    }

    public function test_user_is_allowed_to_create_profession()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->get(route('profession.create'));

        $response->assertStatus(200); // OK
    }

    public function test_user_is_allowed_to_import_profession_for_their_own_character()
    {
        Profession::factory()->create(['name' => 'Influencer']);
        $user = User::factory()->create();
        $character = Character::factory()
            ->for($user)
            ->create(['name' => 'Rambone']);

        $data = '{
            "server":"Bigglesworth",
            "player":"Rambone",
            "guild":"First Blood",
            "profession": {
                "locale":"enUS",
                "name":"Influencer",
                "specializationId":null,
                "specializationName":null,
                "level":375,
                "maxLevel":375,
                "recipes": []
            }
        }';

        $response = $this->actingAs($user)
            ->post(route('profession.store'), ['data' => $data]);

        $response->assertSessionHas(['success' => "Profession imported!"]);
    }

    public function test_user_is_not_allowed_to_import_profession_for_someone_elses_character()
    {
        Profession::factory()->create(['name' => 'Alchemy']);
        $user = User::factory()->create();
        $character = Character::factory()->create(['name' => 'Angus']);

        $data = '{
            "server":"Deviate Delight",
            "player":"Angus",
            "guild":"Phoenix",
            "profession": {
                "locale":"enUS",
                "name":"Alchemy",
                "specializationId":null,
                "specializationName":null,
                "level":375,
                "maxLevel":375,
                "recipes": []
            }
        }';

        $response = $this->actingAs($user)
            ->post(route('profession.store'), ['data' => $data]);

        $response->assertSessionHas(['error' => 'Could not find character "Angus"']);
    }
}
