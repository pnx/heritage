<?php

namespace Tests\Unit\Warcraft;

use PHPUnit\Framework\TestCase;

use App\Warcraft\Races;
use App\Warcraft\Classes;

class RacesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_all()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::GNOME     => 'Gnome',
            Races::NIGHTELF  => 'Night elf',
            Races::DRAENEI   => 'Draenei',
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::TAUREN    => 'Tauren',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->toArray());
    }

    public function test_only_alliance()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::GNOME     => 'Gnome',
            Races::NIGHTELF  => 'Night elf',
            Races::DRAENEI   => 'Draenei',
        ];

        $this->assertEquals($expected, (new Races)->alliance()->toArray());
    }

    public function test_only_horde()
    {
        $expected = [
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::TAUREN    => 'Tauren',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->horde()->toArray());
    }

    public function test_can_be_warrior()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::GNOME     => 'Gnome',
            Races::NIGHTELF  => 'Night elf',
            Races::DRAENEI   => 'Draenei',
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::TAUREN    => 'Tauren',
            Races::UNDEAD    => 'Undead',
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::WARRIOR)->toArray());
    }

    public function test_can_be_paladin()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::DRAENEI   => 'Draenei',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::PALADIN)->toArray());
    }

    public function test_can_be_hunter()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::NIGHTELF  => 'Night elf',
            Races::DRAENEI   => 'Draenei',
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::TAUREN    => 'Tauren',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::HUNTER)->toArray());
    }

    public function test_can_be_rogue()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::GNOME     => 'Gnome',
            Races::NIGHTELF  => 'Night elf',
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::ROGUE)->toArray());
    }

    public function test_can_be_priest()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::DWARF     => 'Dwarf',
            Races::NIGHTELF  => 'Night elf',
            Races::DRAENEI   => 'Draenei',
            Races::TROLL     => 'Troll',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::PRIEST)->toArray());
    }

    public function test_can_be_shaman()
    {
        $expected = [
            Races::DRAENEI   => 'Draenei',
            Races::ORC       => 'Orc',
            Races::TROLL     => 'Troll',
            Races::TAUREN    => 'Tauren'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::SHAMAN)->toArray());
    }

    public function test_can_be_mage()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::GNOME     => 'Gnome',
            Races::TROLL     => 'Troll',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::MAGE)->toArray());
    }

    public function test_can_be_warlock()
    {
        $expected = [
            Races::HUMAN     => 'Human',
            Races::GNOME     => 'Gnome',
            Races::ORC       => 'Orc',
            Races::UNDEAD    => 'Undead',
            Races::BLOODELF  => 'Blood elf'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::WARLOCK)->toArray());
    }

    public function test_can_be_druid()
    {
        $expected = [
            Races::NIGHTELF  => 'Night elf',
            Races::TAUREN    => 'Tauren'
        ];

        $this->assertEquals($expected, (new Races)->class(Classes::DRUID)->toArray());
    }
}
