<?php

namespace Tests\Unit\Warcraft;

use PHPUnit\Framework\TestCase;

use App\Warcraft\Races;
use App\Warcraft\Classes;

class ClassesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_all()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::PALADIN   => 'Paladin',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::SHAMAN    => 'Shaman',
            Classes::MAGE      => 'Mage',
            Classes::WARLOCK   => 'Warlock',
            Classes::DRUID     => 'Druid'
        ];

        $this->assertEquals($expected, (new Classes)->toArray());
    }

    public function test_classes_for_human()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::PALADIN   => 'Paladin',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::MAGE      => 'Mage',
            Classes::WARLOCK   => 'Warlock'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::HUMAN)->toArray());
    }

    public function test_classes_for_dwarf()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::PALADIN   => 'Paladin',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::DWARF)->toArray());
    }

    public function test_classes_for_gnome()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::ROGUE     => 'Rogue',
            Classes::MAGE      => 'Mage',
            Classes::WARLOCK   => 'Warlock'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::GNOME)->toArray());
    }

    public function test_classes_for_night_elf()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::DRUID     => 'Druid'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::NIGHTELF)->toArray());
    }

    public function test_classes_for_draenei()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::PALADIN   => 'Paladin',
            Classes::HUNTER    => 'Hunter',
            Classes::PRIEST    => 'Priest',
            Classes::SHAMAN    => 'Shaman',
            Classes::MAGE      => 'Mage'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::DRAENEI)->toArray());
    }

    public function test_classes_for_orc()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::SHAMAN    => 'Shaman',
            Classes::WARLOCK   => 'Warlock'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::ORC)->toArray());
    }

    public function test_classes_for_troll()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::SHAMAN    => 'Shaman',
            Classes::MAGE      => 'Mage'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::TROLL)->toArray());
    }

    public function test_classes_for_tauren()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::HUNTER    => 'Hunter',
            Classes::SHAMAN    => 'Shaman',
            Classes::DRUID     => 'Druid'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::TAUREN)->toArray());
    }

    public function test_classes_for_undead()
    {
        $expected = [
            Classes::WARRIOR   => 'Warrior',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::MAGE      => 'Mage',
            Classes::WARLOCK   => 'Warlock'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::UNDEAD)->toArray());
    }

    public function test_classes_for_blood_elf()
    {
        $expected = [
            Classes::PALADIN   => 'Paladin',
            Classes::HUNTER    => 'Hunter',
            Classes::ROGUE     => 'Rogue',
            Classes::PRIEST    => 'Priest',
            Classes::MAGE      => 'Mage',
            Classes::WARLOCK   => 'Warlock'
        ];

        $this->assertEquals($expected, (new Classes)->race(Races::BLOODELF)->toArray());
    }
}
