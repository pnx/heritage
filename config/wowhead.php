<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default domain
    |--------------------------------------------------------------------------
    |
    | What wowhead domain to use if not specified. The domain can control
    | versions (classic, ptr, retail) or localization. possible values
    | are: www, ptr, tbc, de, fr, ru, de.classic, ru.tbc etc.
    |
    */

    'domain' => 'tbc',

    /*
    |--------------------------------------------------------------------------
    | Rename links
    |--------------------------------------------------------------------------
    |
    | Set this to true to make wowhead replace the name for all wowhead links
    | on the page.
    |
    */

    'renameLinks' => false,

    /*
    |--------------------------------------------------------------------------
    | Icon Size
    |--------------------------------------------------------------------------
    |
    | Configure the default size of the icons on the site. Available sizes:
    | tiny (the default), small, medium, and large.
    |
    */

    'iconSize' => 'small',

    /*
    |--------------------------------------------------------------------------
    | Iconize Links
    |--------------------------------------------------------------------------
    |
    | Set to true if icons should be added to all links. If you set this to
    | false, you can allow _some_ links on the site to have an icon by
    | setting the `icon-size=<value>` attribute on individual links.
    |
    */

    'iconizeLinks' => true,

    /*
    |--------------------------------------------------------------------------
    | Color Links
    |--------------------------------------------------------------------------
    |
    | Set to true if wowhead should automatically set link text color
    | for all links on the page.
    |
    */

    'colorLinks' => false,

    /*
    |--------------------------------------------------------------------------
    | Disabling components
    |--------------------------------------------------------------------------
    |
    | You can disable some components in the tooltip by setting the component
    | in question to true.
    |
    */

    "hide" => [
        // "maxstack" => true,
        // "droppedby" => true,
        // "dropchance" => true,
        // "reagents" => true,
        // "ilvl" => true,
        // "extra" => true,
        // "sellprice" => true
    ],

];
