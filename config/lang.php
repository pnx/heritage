<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language configuration
    |--------------------------------------------------------------------------
    |
    | Define available languages as locale (key), name (value) pairs.
    |
    */

    'en' => 'English',
    'se' => 'Svenska',

];
